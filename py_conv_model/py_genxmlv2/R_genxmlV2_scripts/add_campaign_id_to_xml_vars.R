#' Add the campaign_id to the xml_vset_var and xml_vset_src values that need
#' them based on the var_group
#' 
#' @param xml_vset_vars_df a data frame containing at least `var_group`,
#' `xml_vset_var` and `xml_vset_src` columns
#' @param campaign_id the campaign_id to add if needed
#' @return a modified data frame, where the campaign_id has been appended
#' to the xml_vset_var and xml_vset_src columns when var_group is one of
#' the variables that needs a campaign_id
#' @export

add_campaign_id_to_xml_vars <- 
  function(xml_vset_vars_df, campaign_id = 'REPLACE_WITH_CAMPAIGN_ID') {
  # List of variable names that need to have the campaign id appended to.
  # See java code for complete list
  # TODO find reference in java code to fill out this list
  need_campaign_id <- c('lifetimecampaignimpressions', 
                        'daycampaignimpressions',
                        'impressionlagminutes')
  xml_vset_vars_df %>%
    mutate(xml_vset_var = 
             ifelse(var_group %in% need_campaign_id, 
                    paste(xml_vset_var, campaign_id, sep = '_'), 
                    xml_vset_var),
           xml_vset_src =
             ifelse(var_group %in% need_campaign_id,
                    paste(xml_vset_src, campaign_id, sep = '.'),
                    xml_vset_src)
           )
}
