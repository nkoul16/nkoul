#' Create a 'range' type variable node for a variable-set
#' 
#' @param variable_name the name of the node
#' @param min_rng the min value of the range (inclusive)
#' @param max_rng the max value of the range (inclusive)
#' @param value the value of the variable if it matches
#' @return a 'variable' xmlNode of type 'match' (child of a 'variable-set' node)
#' @seealso create_vset_match_node
#' @export

create_vset_range_node <- function(variable_name, 
                                   min_rng, 
                                   max_rng, 
                                   value = '1') {
  newXMLNode('variable', 
             attrs = c(name = variable_name, 
                       min = min_rng,
                       max = max_rng,
                       value = value)
  )
}
