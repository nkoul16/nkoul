#' Merge loyalty club info into factor_names
#' 
#' The loyalty clubs are defined in the function source code
#' 
#' @param factor_names data frame  with which to merge the loyalty info. 
#' Must have at least `fac_name` and `var_group` columns
#' @return modified factor_names, adding several columns (min_rng, max_rng, 
#' isMatchVar, match, vname, default_value, sortby)
#' @export

# TODO check for empty input df
merge_in_loyalty_clubs <- function(factor_names) {
  # Define loyalty clubs
  # TODO - put these in an external file?
  
  hilton <- list(
    Blue = 'Blue',
    Silver_VIP = 'Silver',
    Gold_VIP = 'Gold',
    Diamond_VIP = 'Diamond'
  )
  
  ihg <- list(
    Club = c('CLUB'),
    Gold = c('GOLD'),
    Platinum = c('PLATINUM')
  ) 
  
  mrt <- list(
    Basic = c('B', 'R', 'Member'),
    Silver = c('S', 'SL', 'Silver'),
    Gold = c('G', 'GL', 'Gold'),
    Platinum = c('P', 'PL', 'Platinum'),
    Platinum_Premier = c('PP', 'Platinum Premier')
  )
  
  ritz <- list(
    Basic = c('B', 'R'),
    Silver = c('S'),
    Gold = c('G'),
    Platinum = c('P')
  )
  
  # airline
  usair <- list(
    Basic = 'B',
    Silver = 'S',
    Gold = 'G',
    Platinum = 'P',
    Chairman = 'C'
  )
  
  united <- list(
    Basic = c('General', 'GN', 'Member', 'Membro', 'Miembro', 
              'Mitglied', 'Socio', 'Associado'),
    Premier_Silver = c('Silver', 'Premier Silver', '2P', 'Premier'),
    Premier_Gold = c('Gold', '1P', 'Premier Executive', 
                     'Premier Gold', 'Premier Oro'),
    Premier_Platinum = c('Platinum', 'Premier Platinum'),
    Premier_1K = c('1K', 'Premier 1K'),
    Global_Services = c('Global Services', 'Global', 'GS')
  )
  
  delta <- list(
    SkyMiles_Member = c('BA', 'SkyMiles Member', 'FF', 'FF=1', 'FF=2', 'FF=3', 
                        'FF=4', 'FF=5', 'FF=6', 'FF=7', 'FF=8', 'FF=9'),
    Silver_Medallion = c('Silver', 'Silver_Medallion', 
                         'Silver_Medallion=1', 'Silver_Medallion=2', 
                         'Silver_Medallion=3', 'Silver_Medallion=4', 
                         'Silver_Medallion=5', 'Silver_Medallion=7', 
                         'Silver_Medallion=8',
                         'FO', 'FO=1', 'FO=2', 'FO=3', 'FO=4', 'FO=5', 
                         'FO=6', 'FO=7', 'FO=8', 'FO=9', 'FO=12'),
    Gold_Medallion = c('Gold', 'GM', 'GM=1', 'GM=2', 'GM=3', 'GM=4', 
                       'GM=5', 'GM=6', 'GM=7', 'GM=8',
                       'Gold_Medallion', 'Gold_Medallion=1', 
                       'Gold_Medallion=2', 'Gold_Medallion=3', 
                       'Gold_Medallion=4', 'Gold_Medallion=5', 
                       'Gold_Medallion=6'),
    Platinum_Medallion = c('Platinum', 'PM', 'PM=1', 'PM=2', 
                           'PM=3', 'PM=4', 'PM=5', 'PM=6', 
                           'PM=7', 'PM=8', 'PM=9',
                           'Platinum Medallion', 'Platinum Medallion=1', 
                           'Platinum Medallion=2', 'Platinum Medallion=3', 
                           'Platinum Medallion=4', 'Platinum Medallion=5'),
    Diamond_Medallion = c('Diamond', 'DM', 'DM=1', 'DM=2', 'DM=3', 
                          'DM=4', 'DM=5', 'DM=6', 'DM=7', 'DM=8', 'DM=9',
                          'Diamond Medallion', 'Diamond Medallion=1', 
                          'Diamond Medallion=2', 'Diamond Medallion=3', 
                          'Diamond Medallion=4', 'Diamond Medallion=5')
  )
  
  virgin <- list(Member='yes')
  
  hilton_df <- list2kv(hilton)
  ihg_df <- list2kv(ihg)
  mrt_df <- list2kv(mrt)
  ritz_df <- list2kv(ritz)
  usair_df <- list2kv(usair)
  united_df <- list2kv(united)
  delta_df <- list2kv(delta)
  virgin_df <- list2kv(virgin)
  
  all_loyalty_df <- rbind(hilton_df, ihg_df, mrt_df, ritz_df, usair_df, 
                          united_df, delta_df, virgin_df)
  
  # ----- Now merge in all_loyalty_df
  if (!has_columns(factor_names, 
                   c('fac_name', 'var_group', 'default_val_type'),
                   emptyDfOk = FALSE)) {
    warning("factor_names is missing necessary columns")
    data.frame()  
  } else {
    factor_names %>%
      filter(default_val_type == 'loyalty') %>%
      mutate(fac_name_us = spc2underscore(fac_name)) %>%
      left_join(all_loyalty_df, 
                by = c('var_group' = 'name', 'fac_name_us' = 'key')) %>%
      mutate(min_rng = NA_character_,
             max_rng = NA_character_,
             isMatchVar = TRUE,
             match = value,
             vname = paste(var_group, fac_name_us, sep = '_'),
             default_value = 1,
             sortby = vname, match) %>%
      select(-value)
  }
}
