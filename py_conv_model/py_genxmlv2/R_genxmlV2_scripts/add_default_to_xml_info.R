#' Add default values to xml info data.frame
#' 
#' The resulting data frame will contain all information needed
#' to create a model template xml file.
#' 
#' add rows for the default values based on the default_val_type (count, recency, 
#' loyalty or true_factor).
#' 
#' Also removes Loyalty / NonMember rows from data frame.
#' 
#' Remove NA's from mod_estimate
#' 
#' @param xml_info_df a data.frame containing at least the following columns:
#' `avg_estimate`, `fac_name`, `match`, `max_rng`, `min_rng`, `mod_estimate`,
#' `print_order`, `var_group`, `default_val_type`, `vname`, `xml_vset_var`
#' @return the input df with additional rows for defaults
#' @export

add_default_to_xml_info <- function(xml_info_df) {
  # Break with error message if all necessary columns aren't here
  has_columns(mydata = xml_info_df, 
                    cols2check = c('avg_estimate',
                                   'fac_name',
                                   'match',
                                   'max_rng',
                                   'min_rng',
                                   'mod_estimate',
                                   'print_order',
                                   'var_group',
                                   'default_val_type',
                                   'vname',
                                   'xml_vset_var'))
  
  # add a column to indicate if default value or not
  # use this to filter by when creating xml
  xml_info_df <- mutate(xml_info_df, is_default = FALSE)
  
  # true_factors: set estimate to 0 or avg_estimate
  true_factor_rows <- xml_info_df %>% 
    group_by(var_group) %>%
    filter(min_rank(print_order) <= 1,
           print_order > 0,
           default_val_type == 'true_factor') %>%
    mutate(print_order = print_order - 0.5,
           vname = paste0(xml_vset_var, '_default'),
           min_rng = NA,
           max_rng = NA,
           match = NA,
           is_default = TRUE) %>%
    ungroup() %>%
    mutate(mod_estimate = ifelse(fac_name == '0', 0, avg_estimate))  
  
  # count_rows: set default = 0
  count_rows <- xml_info_df %>% 
    group_by(var_group) %>%
    filter(min_rank(print_order) <= 1,
           print_order > 0,
           default_val_type == 'count') %>%
    mutate(print_order = print_order - 0.5,
           vname = paste0(xml_vset_var, '_default'),
           min_rng = NA,
           max_rng = NA,
           match = NA,
           mod_estimate = 0,
           is_default = TRUE) %>%
    ungroup()
  
  # recency_rows: set default coef to last coef (max time, rows already sorted)
  recency_rows <- xml_info_df %>%
    group_by(var_group) %>%
    mutate(tmp_print_order = min(print_order)) %>%  # temp column
    filter(min_rank(desc(print_order)) == 1, 
           default_val_type == 'recency') %>%
    mutate(print_order = tmp_print_order - 0.5,
           vname = paste0(xml_vset_var, '_default'),
           min_rng = NA,
           max_rng = NA,
           match = NA,
           is_default = TRUE) %>% 
    select(-tmp_print_order) %>%
    ungroup()
  
  # loyalty_rows: set default coef to that of 'NonMember' level
  loyalty_rows <- xml_info_df %>%
    group_by(var_group) %>%
    mutate(print_order = min(print_order) - 0.5,
           vname = paste0(xml_vset_var, '_default'),
           min_rng = NA,
           max_rng = NA,
           match = NA,
           is_default = TRUE) %>%
    filter(default_val_type == 'loyalty',
           fac_name == 'NonMember') %>%
    ungroup()
  
  # unknown_rows: set default coef to that of 'Unknown' level
  unkwown_rows <- xml_info_df %>%
    group_by(var_group) %>%
    mutate(print_order = min(print_order) - 0.5,
           vname = paste0(xml_vset_var, '_default'),
           min_rng = NA,
           max_rng = NA,
           match = NA,
           is_default = TRUE) %>%
    filter(default_val_type == 'unknown',
           fac_name == 'Unknown') %>%
    ungroup()
  
  default_rows <- rbind(true_factor_rows, 
                        count_rows, 
                        recency_rows, 
                        loyalty_rows,
                        unkwown_rows)
  
  xml_info_df <- rbind(xml_info_df, default_rows)
  xml_info_df[which(is.na(xml_info_df[, 'mod_estimate'])), 'mod_estimate'] <- 0
  
  #----- remove rows from xml_info_df that only served to get the default value
  # These rows has served their purpose. They created the _default rows.
  # don't create variable or coefficient for them
  xml_info_df <- xml_info_df %>%
    filter(!(!is_default & fac_name == 'NonMember' & default_val_type == 'loyalty')) %>%
    filter(!(!is_default & fac_name == 'Unknown' & default_val_type == 'unknown')) %>%
    filter(!(!is_default & match == 'other' & var_group == 'lst_dstnLev')) %>%
    filter(!(!is_default & match == 'other' & var_group == 'country_statelev')) %>%
    filter(!(!is_default & match == 'other' & var_group == 'domainlev')) %>%
    filter(!(!is_default & match == 'other' & var_group == 'metrocodelev'))
  
  #----- return xml_info_df with all the rows in printing order
  arrange(xml_info_df, var_group, print_order)
}

