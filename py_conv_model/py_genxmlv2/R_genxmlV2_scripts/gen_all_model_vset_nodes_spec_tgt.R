#' Given a fit object, generates all the variable-set nodes
#' 
#' @param vn_df a variable nodes data.frame containing `vname`, `min_rng`, 
#' `max_rng`, `match`, `default_value`, and `print_order`, `xml_vset_src`, 
#' `xml_vset_src`, `is_default`, `spec_tgt` columns
#' @return a list of variable-set nodes to append to p/transforms/variables
#' @export

gen_all_model_vset_nodes_spec_tgt <- function(vn_df) {
  
  has_columns(vn_df, c('vname', 'min_rng', 'max_rng', 'match', 
                       'default_value', 'print_order', 'xml_vset_var',
                       'xml_vset_src', 'is_default', 'var_group', 'spec_tgt'))
  # helper function
  get_match_or_range_node <- function(vname, min_rng, max_rng, 
                                      match, default_value, spec_tgt = NA) {
    
    # if it has special_targeting, change vname
    if (!is.na(spec_tgt)) {
      vname <- paste(vname, '_intermediate', sep = '')
    }
    if (is.na(min_rng)) {
      create_vset_match_node(variable_name = vname, 
                             match_string = match, 
                             value = default_value)
    } else {
      create_vset_range_node(variable_name = vname, 
                             min_rng = min_rng, 
                             max_rng = max_rng, 
                             value = default_value)
    }
  }
  
  # helper function
  get_spec_tgt_ref_node <- function(var_group, vname) {    
    set_op_node <- newXMLNode(name = 'variable-set-operation', 
                              attrs = c(name = vname, 
                                        operator = 'and'))
    ref_node_1 <- newXMLNode(name = 'reference',
                             attrs = c(name = paste0(var_group, '_value_intermediate')))
    ref_node_2 <- newXMLNode(name = 'reference',
                             attrs = c(name = paste0(vname, '_intermediate')))
    ref_node <- addChildren(set_op_node, kids = append(ref_node_1, ref_node_2))
    ref_node
  }
  
  
  # helper function - operates on each variable group
  # use as.character... because x$xml_vset_var is a factor
  operate_on_var_group <- function(x) {
    
    if (!is.na(x[1, ]$spec_tgt)) {
      # if spec_tgt not NA 
      # create match note (like for sepecial marsha codes)
      xml_var_spec_tgt <- paste(as.character(x[1, ]$xml_vset_var), '_value_intermediate', sep = '')
      xml_src_spec_tgt <- sub('[a-zA-Z]*_', 'ckv_', as.character(x[1, ]$xml_vset_src))
      vset_node_spec_tgt <- create_vset_node(xml_var_spec_tgt, xml_src_spec_tgt)
      spec_tgt_val <- strsplit(as.character(x[1, ]$spec_tgt), '\\|')[[1]]
      # create match nodes:
      xs <- mapply(create_vset_match_node, xml_var_spec_tgt, 
                                     spec_tgt_val, 1)
      comment_node <- newXMLCommentNode(paste0(xml_var_spec_tgt, '_vset'))
      ks <- list(comment_node)
      ks <- append(ks, xs)
      vset_node_spec_target <- addChildren(vset_node_spec_tgt, kids=ks)
      
      # create the main node:
      xml_var <- paste(as.character(x[1, ]$xml_vset_var), '_intermediate', sep = '')
      xml_src <- as.character(x[1, ]$xml_vset_src)
      vset_node <- create_vset_node(xml_var, xml_src)
      # TODO make sure in order
      xs <- mapply(get_match_or_range_node, paste0(as.character(x$vname), '_intermediate'), 
                   x$min_rng, x$max_rng, 
                   as.character(x$match), x$default_value)
      comment_node <- newXMLCommentNode(paste0(xml_var, '_vset'))
      ks <- list(comment_node)
      ks <- append(ks, xs)
      vset_node <- addChildren(vset_node, kids=ks)

      # create reference default node:
      xml_var <- as.character(x[1, ]$xml_vset_var)
      default_node <- newXMLNode(name = 'variable-set-operation', 
                                 attrs = c(name = paste0(xml_var, '_default'), 
                                           operator = 'max'))
      ref_node_1 <- newXMLNode(name = 'reference',
                               attrs = c(name = paste0(xml_var, '_value_intermediate_default')))
      ref_node_2 <- newXMLNode(name = 'reference',
                               attrs = c(name = paste0(xml_var, '_intermediate_default')))
      default_node <- addChildren(default_node, kids = append(ref_node_1, ref_node_2))
      names(default_node) <- NULL
      
      # create other reference nodes:
      ref_node <- mapply(get_spec_tgt_ref_node, as.character(x$var_group), 
                         as.character(x$vname))
      names(ref_node) <- NULL
      
      # combine all nodes:
      vset_node_spec_target <- append(vset_node_spec_target, 
                                      append(vset_node, 
                                             append(default_node, ref_node)
                                             )
                                      )
      # vset_node_spec_target is a list of several elements 
      # so need to make it one big list as one element
      vset_node_spec_target <- list(vset_node_spec_target)
    } else {
      xml_var <- as.character(x[1, ]$xml_vset_var)
      xml_src <- as.character(x[1, ]$xml_vset_src)
      vset_node <- create_vset_node(xml_var, xml_src)
      # TODO make sure in order
      xs <- mapply(get_match_or_range_node, as.character(x$vname), 
                   x$min_rng, x$max_rng, 
                   as.character(x$match), x$default_value)
      comment_node <- newXMLCommentNode(paste0(xml_var, '_vset'))
      ks <- list(comment_node)
      ks <- append(ks, xs)
      vset_node <- addChildren(vset_node, kids=ks)
      vset_node <- list(vset_node)
    }
  }
  
  tmp_vset <- vn_df %>%
    filter(vname != 'BETA_NULL' & !is_default) %>%
    select(var_group, xml_vset_var, xml_vset_src, 
           vname, min_rng, max_rng, match, default_value, spec_tgt) %>%
    # TODO do I need default_value in the group_by?
    group_by(xml_vset_var, xml_vset_src, default_value) %>%
    do(vset = operate_on_var_group(.))
  
  unlist(tmp_vset %>% .[['vset']])
}
