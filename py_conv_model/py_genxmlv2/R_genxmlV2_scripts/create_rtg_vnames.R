#' Create the variable names and estimates for retargeting (RTG) nodes
#' 
#' @param v base name (defaults to RTG)
#' @param estimates estimate values to use (defaults to c(2.5, 0.75, 0.5))
#' @return a data.frame with vname, estimate, min_rng, max_rng and value
#' @export

create_rtg_vnames <- function(v = 'RTG', estimates = c(2.5, 0.75, 0.5)) {
  # define ranges as a list, then convert to data.frame
  lsts <- list(c(0, 1), 
               c(2, 7), 
               c(8, 999999999))
  rngs <- lists2df(lsts, col_names = c('min_rng', 'max_rng'))
  rngs$estimate <- estimates
  
  
  # fill out the values that CreateVsetRangeNode needs
  rngs <- mutate(rngs, 
                 vname = paste(v, min_rng, max_rng, sep = '_'),
                 value = 1)
  select(rngs, vname, estimate, min_rng, max_rng, value)
}
