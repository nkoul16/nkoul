#' Create a name for a variable-set-operation based on window number and key_id
#' for hand crafted models 
#'  
#' @param window_num the window number
#' @param key_id the key id
#' @param prefix the prefix to use
#' @return the standard name (prefix_windowNum_keyId)
#' @export

create_handcrafted_vso_name <- function(window_num, key_id, prefix) {
  xs <- c(prefix, window_num, key_id)
  filter_and_concat(xs, '_')
}
