# convert_all_cols_2_factors.R
def convert_all_cols_2_categories(mydata, cols_2_ignore= []):
    """
    Function works on DataFrame inplace
        
    converts all columns that are not dtype category
    to category dtype
    
    excludes columns listed in @cols_2_ignore from 
    type conversion
    
    """
    not_factor_columns= mydata.select_dtypes(exclude= ['category']).columns.tolist()
    
    # exclude cols in @cols_2_ignore
    if len(cols_2_ignore) > 0:
        not_factor_columns= [ col for col in not_factor_columns if col not in cols_2_ignore ]
    
    if len(not_factor_columns) > 0:
        for col in not_factor_columns:
            mydata[col]= mydata[col].astype('category')