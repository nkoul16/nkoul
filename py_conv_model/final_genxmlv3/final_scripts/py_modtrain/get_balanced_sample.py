def get_balanced_sample(mydata, max_samples = 250000, neg_pos_ratio = 10, fixed_ratio = False):
    """
    fixed_ratio == True forces the balanced sample
    to strictly follow the given ratio
    
    else the neg_pos_ratio is a celiling
    
    
    Need to finish commenting
    """
    
    
    assert neg_pos_ratio >= 1 
    
    loc_data = mydata.copy()
    # omit na's
    loc_data.dropna(how= 'any', axis= 0, inplace= True)

    # Reset the index 
    # new index will be used to select 
    loc_data.reset_index(inplace= True, drop= True)
  

    # _0, _1 refer to negative and positive indices and counts for the
    #   unbalanced data
    # _neg, _pos refer to the balanced values
    idx_1 = loc_data.query('target == 1').index.to_numpy()
    idx_0 = loc_data.query('target == 0').index.to_numpy()
    
    # Throw an error if either all positive or all negative samples
    assert len(idx_1) != 0, 'no positive examples in data set'
    assert len(idx_0) != 0, 'no negative examples in data set'

    # don't let the sample have more positive samples than negative
    num_0= len(idx_0)
    num_1= min(num_0, len(idx_1))
    


    # you can't have more samples than your data set has rows
    adj_max_samples= min(  min( max_samples, len(loc_data) ) 
                          ,np.floor( num_1 * (1 + neg_pos_ratio) )
                        )
    
    
    

  
    # sample positive values if there are too many (min ratio = 1, max ratio
    # may or may not be fixed)
    
    if fixed_ratio:
        if num_1 * (neg_pos_ratio + 1) > adj_max_samples:
            # if the ratio times the num of positives is 
            # larger than the maximum number of samples 
            # Need to sample positive and negative responses
            num_pos = math.ceil(adj_max_samples / (neg_pos_ratio + 1))
            num_neg = adj_max_samples - num_pos
        else:
            # otherwise only resample the negative resposnes
            # the size of the negative samples will be 
            # FIXED by the neg_pos_ratio
            num_pos = num_1
            num_neg = math.floor(num_pos * neg_pos_ratio) 

        if num_neg > num_0:
            # 
            # in case there aren't enough negative values for this ratio
            num_neg = min(num_0, ceiling(adj_max_samples * neg_pos_ratio / (1 + neg_pos_ratio)))
            num_pos = math.ceil(min(adj_max_samples - num_neg, num_neg / neg_pos_ratio))
            
            # TODO: check whether the following will work instead of the above
            # num_neg <- num_0
            # num_pos <- ceiling(num_neg / neg_pos_ratio)
    else:
        if num_1 > (adj_max_samples / 2):
            # Need to sample positive and negative responses
            num_pos = round(adj_max_samples / 2)
            num_neg = adj_max_samples - num_pos
            if num_neg > num_0: 
                num_pos = num_neg
                num_neg = num_0
        else:
            # Only sample negative responses if needed
            num_pos = num_1
            num_neg = min(num_0, min(num_pos * neg_pos_ratio, adj_max_samples - num_pos))
            # TODO check whether if statement can be deleted below:
            if num_pos > num_neg:
                num_pos = num_neg



    np.random.seed(seed=16)

    # WATCH OUT!! if the set to be sampled only has 1 element, it will 
    # be treated as a list of length n, where n is the value of that element
  
    idx_pos = np.random.choice(idx_1, size = num_pos, replace= False)
    idx_neg = np.random.choice(idx_0, size = num_neg, replace= False)

    idx_combined = np.r_[idx_pos, idx_neg]

    loc_data = loc_data.iloc[idx_combined]
    
    
    return loc_data
