def get_variable_bin_types(path='test_files/'):
    """
    csv's used:    variable-bin-type.csv
                  ,variable-bin-type-ext.csv
                  ,product-variable-bin-type-source.csv
                  
        **all 3 currently located in modtrain/inst/extdata/
    
    
    Get mapping information from csv's
        mapping from variable names to binning type
    
    
    Turn mappings into a DataFrame
    
    Return DataFrame containing: ['var_name', 'cnt_rcy_flag', 'bin_type']
            
        df['cnt_rcy_flag'] is either 'count' or 'recency'
        df['bin_type'] can be 'count', 'day', 'minute', 'day180'

    """
    
    def df_strip(df):
        """
        Strips whitespace from all dtype: object columns (this is how pandas saves columns with strings be default)  
        Strips the column's name and values
        """
        df = df.copy()
        for i in df.columns:
            if df[i].dtype == np.object:
                df[i] = pd.core.strings.str_strip(df[i])
                df = df.rename(columns={i:i.strip()})
        
        return df


    fname_old= path + 'variable-bin-type.csv'
    fname_new= path + 'variable-bin-type-ext.csv'
    fname_prod_vars= path + 'product-variable-bin-type-source.csv'

    # Turn files into DataFrames
    old_vars = pd.read_csv(fname_old, header= 0, sep= ',')
    new_vars = pd.read_csv(fname_new, header= 0, sep= ',')
    product_vars = pd.read_csv(fname_prod_vars, header= 0, sep=',', usecols= [0,1,2])

    # Strip Whitespace from values and header
    old_vars = df_strip(old_vars)
    new_vars = df_strip(new_vars)
    product_vars = df_strip(product_vars)


    # Concat dataframes
      # TODO - what if duplicates? conflicting info?
    df = pd.concat([old_vars,new_vars,product_vars], ignore_index = True)
    
    return df

         
