def print_data_dimensions_click(mydata):
    """
    finds the shape of input DataFrame @mydata
    and the number of Target 1's as num_clicks
    
    Prints out dimension and click information
    
    Returns dictionary @res containing the info
    printed out
    
    ** If @mydata is empty will return an error
    """
    
    n_rows = len(mydata.index)
    n_cols = len(mydata.columns)
    
    assert n_cols != 0 or n_rows != 0, "The data frame is empty."
    
    # Number of Converters is = Number of Clicks
    # strange but ok??
    cs= len(mydata.query('target == 1').index)
    
    print("Dimensions = {rows} x {columns}".format(rows= n_rows, columns= n_cols))
    print("Number of clicks = {}".format(cs))
  
    res= {}
    res['num_rows_total'] = n_rows
    res['num_cols'] = n_cols
    res['num_clicks'] = cs
    
    return res