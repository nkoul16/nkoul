def compute_save_bias_correction(mydata, train, path = './', file = 'bias-correction-values.csv'):
    """
    Creates a csv containg the parameters
    computed to calculate the bias correction
    
    Returns DataFame bias_corrections
    
    ** May need to add has_columns call to ensure 'target' column exists
    """
    
    
    
#' Compute the bias correction due to sampling
#' 
#' returns a data.frame with pi0, pi1, rho0, rho1, intercept.corr
#' 
#' @param mydata full data set (must have at least 'target' col)
#' @param train training set (subset of mydata, must have at least 'target' col)
#' @param path output directory (defaults to cur directory)
#' @param file name of output file (default = bias-correction-values.csv)
#' @return the bias correction values as a data.frame
#' @export


  # Calculate pi & rho parameters 
    def positive_fraction(df):
        """
        used to calculate positive_fraction 
        pi for the bias correction formula
        
        
        """
        return len(df[df['target'] == 1].index)/len(df.index)
        
    # Calculate pi & rho parameters 
    # for both the training set @train
    # and the entire dataset @mydata
    
    pi_1 = positive_fraction(mydata)
    pi_0= 1 - pi_1

    rho_1 = positive_fraction(train)
    rho_0 = 1 - rho_1
  
    intercept_corr = math.log((pi_0 * rho_1) / (pi_1 * rho_0))
  
    # save the calculated values into a DataFrame
    bias_corrections = pd.DataFrame(data = {'value':[pi_0, pi_1, rho_0, rho_1, intercept_corr]},
                                    index = ['pi0', 'pi1', 'rho0', 'rho1', 'intercept_corr']
                                   ) 
    # save the DataFrame to csv
    file_name = path + file
    bias_corrections.to_csv(file_name)
    
    return bias_corrections
    
