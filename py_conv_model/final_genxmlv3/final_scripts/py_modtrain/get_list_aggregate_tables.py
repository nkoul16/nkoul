def get_list_aggregate_tables(mydata, print2screen = True):
    """
    get conversion_rate ('avgOfTarget'), total conversions ('tgtOneCnt'),
    and total impressions ('impCnt') for each category in each column
    
    if print2screen == True then results will be printed to screen
    
    return a dictionary @tables of dataframes with 
    column/variable names as keys and 
    the aggregated results for each column as values
    
    
    """
    
    # TODO add net conversion rate per variable?
    

    def get_aggregates(mydata, name):
        """
        get aggregated data for a given column in a dataframe
        """
        
        table = mydata.groupby(name, as_index=False).target.agg({  'avgOfTarget':'mean'
                                                                  ,'tgtOneCnt':'sum'
                                                                  ,'impCnt': 'count'
                                                                }
                                                               )          

        table.sort_index(inplace = True)
        return table
    
    
    if print2screen:
        print_timestamp('Begin get_list_of_aggregate_tables')

    
    col_names = sorted(mydata.columns.tolist())
    tables = {}
    
    
    # For each col in @mydata 
    # call @get_aggregates() and store the 
    # results in a dictionary
    # then print out results to screen 
    for i,col in enumerate(col_names):
        print(col)
        tables[col]= get_aggregates(mydata, col)
        if print2screen:
            print("[[{}]]".format(i))
            print(tables[col].head())
            print()
    
    return tables
