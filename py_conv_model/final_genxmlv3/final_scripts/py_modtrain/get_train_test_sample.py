# get_train_test_sample.R

def get_train_test_sample(mydata, pct_of_train= 0.80):
    """
    @pct_of_train is the percent of @mydata that 
    is sampled into the training set
        
        sampled without replacement
    
    Retrun a dictionary of DataFrames
        contains sampled train and test sets

        keys: value
            'train': train_df
            'test': test_df
            
            
    ** Could also just return both DataFrames individually as a tuple
    
    """
    
    loc_data= mydata.copy()
    
    loc_data.dropna(how='any', axis=0, inplace=True)
    
    # reset the index so it can be sampled 
    # drop= True removes the old_index from the dataframe
    loc_data.reset_index(inplace= True, drop= True)
    
    
    
    # get the number of rows in the training sample
    # = number of rows in dataset * @pct_of_train
    n_row_data= len(loc_data)
    n_row_train= math.ceil(n_row_data * pct_of_train)
    
    np.random.seed(16)
    
    # Take a random sample of index
    # resetting the index sets it to the equivalent
    # of np.arange(# row in DataFrame)
    idx_train= np.random.choice(np.arange(n_row_data), n_row_train)
    
    # sample loc_data 
    train= loc_data.iloc[idx_train]
    
    # test sample is all index values not in idx_train
    test= loc_data.iloc[ ~loc_data.index.isin(idx_train) ]
    
    # save both train and test sets into a dictionary
    train_test_data= {}
    train_test_data['train']= train
    train_test_data['test']= test

    return train_test_data
    

