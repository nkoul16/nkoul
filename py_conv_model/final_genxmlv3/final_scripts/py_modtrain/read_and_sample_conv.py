def read_and_sample_conv(file, N = 5, max_samples = 6e6):
    """
    Read a data.csv with sep= '|' to mydata
    Sample up to max_samples rows from mydata
    Duplicate click-conversions N times
    Take sample and drop rows with ANY NA values

    Return Sampled Df 
    """
    
    # print_timestamp is a function from adaratools
    print_timestamp("Start R script")

    
    # Read in data.csv.gz
    # Can modify compression if datafile is uncompressed or in a different format
    # low_memory= True is for comp w/ less memory 
    ## Should probably set it to false since dtypes are undefined
    ## Can define datatypes if necessary  
    mydata= pd.read_csv(file, sep='|', header= 0, compression= 'gzip')
    
    
    # Set all columns to lowercase
    mydata.columns = mydata.columns.str.lower()

    print("Column names: raw data =")
    print(mydata.columns)
    
    # Get # of Samples to take
    # Minimum of @n_rows in mydata and @max_samples
    n_rows= len(mydata.index)
    num_samples= np.minimum(n_rows, max_samples).astype('int')
    
    if num_samples == max_samples:
        # Set seed for sampling and Resample the index of mydata @num_samples times
        np.random.seed(16)
        samples_idx= np.random.choice(a= mydata.index, size= num_samples, replace= False)
    
    else:
        # sample_idx = num_samples so dataframe index length (n_rows) remains unchanged
        samples_idx= mydata.index
    
  
    print("Randomly select {} samples from the raw data.".format( num_samples ))

    # Create a slice to grab indicies from 
    # Copy is unnecessary becasue we are not modifying the slice
    sample_df= mydata.iloc[samples_idx][['target','convtype']].copy()
    
    
    # Select indices of negative response, conversions and clicks
    idx_0= sample_df.query('target == 0').index
    idx_v= sample_df.query('target == 1 & convtype == "V"').index
    idx_c= sample_df.query('target == 1 & convtype == "C"').index  
    
    
    # Print out sampling Information
    print("{} rows with target == 0".format( len(idx_0) ))
    print("{} rows with target == 1 and convtype == 'V'".format( len(idx_v) ))
    print("{} rows with target == 1 and convtype == 'C'".format( len(idx_c) )) 
    print("Duplicate click conversion rows {} times.".format( N ))
    
    
    # Duplicate click conversions @N times
    idx_c= np.repeat(idx_c, N)
    
    # Combine all sample Indicies
    idx_combined= np.concatenate((idx_0,idx_v,idx_c))
                                 
    # Create new Data Frame from sample
    mydata= mydata.iloc[idx_combined]
    
    # Fill NA's in convtype column so all tgt_0's are not dropped 
    mydata.fillna(value = {'convtype':''}, inplace= True)
    
    # drop NA's
    mydata.dropna(axis= 0, how= 'any', inplace= True)

    print_timestamp("Finish sampling data")

    return mydata

