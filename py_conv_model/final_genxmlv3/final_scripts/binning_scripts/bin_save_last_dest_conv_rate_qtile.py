def bin_save_last_dest_conv_rate_qtile(mydata, path = './', file = 'conv-rate-level-last-dstn.csv', print2screen= True):
    """
    Bin the last destination searched (lst_dstn) by conversion rate and quantile
    assigning each destination to a bin (lst_dstnLev)
    
    Rank popular_destinations by conversion rate, and create 20 levels by quantile 
    (5%, 10%, ...)
    
    popular_destinations criteria:
       >= 5000 Impressions
           OR
       >= 100 Impressions AND Conversion Rate >= 0.03
       
    Outputs:
        conv-rate-level-last-dstn.csv
            - conversion rate and impression counts for all destinations with 
              'NOWHERE' and non popular destinations grouped together as 'other'
            
        conv-rate-level-last-dstn.csv-raw
            - conversion rate and impression counts for all ungrouped ("Raw") destinations 
       
       
       Returns aggs
           - Dataframe used to create conv-rate-level-last-dstn.csv
           
        ** May need to return mydata as well if any changes are not made inplace
    """
    
    # has_columns is a function from adaratools
    has_columns(mydata, ['target', 'lst_dstn'])
    
    # convert all lst_dstn values to all caps to avoid problems like 'BOM' v 'Bom'
    mydata['lst_dstn']= mydata['lst_dstn'].str.upper()
    
    
    # Create copy aggs_all
    aggs_all= mydata[['lst_dstn','target']].copy()
    
    # Group by country_state and for each
    # get conversion rate:        conv_rate= mean(target)
    # get number of impressions:  num_at_location = count
    aggs_all= aggs_all.groupby(by= 'lst_dstn', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                            ,'num_to_dstn':'count'
                                                                        })
    
    
    # Sort by: 
    #    conv_rate: ascending
    #    num_to_dstn: descending
    aggs_all.sort_values(by= ['conv_rate','num_to_dstn'], ascending= [True, False], inplace= True)

      
        
    # get a list of the most popular destinations
    popular_destinations= aggs_all.query('(num_to_dstn >= 3000) | ((num_to_dstn >= 100) & (conv_rate >= 0.03))'
                                     )['lst_dstn'].array     

        
    # assign lst_dstn to 'other' for 
    # all destinations not in popular_destinations  
    # assign lst_dstn == 'NOWHERE' to 'other' as well
    mydata.loc[~mydata['lst_dstn'].isin(popular_destinations), 'lst_dstn']= 'other' 
    mydata.loc[ mydata['lst_dstn'] == 'NOWHERE', 'lst_dstn']= 'other' 



    # Create copy aggs
    # Different from aggs_all becasue
    # All lst_dstn not in popular_destinations and 'NOWHERE' have been set to 'other'
    aggs= mydata[['lst_dstn','target']].copy()
    
    aggs= aggs.groupby(by= 'lst_dstn', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                    ,'num_to_dstn':'count'
                                                                })
    
    # Sort by: 
    #    conv_rate: ascending
    #    num_to_dstn: descending
    aggs.sort_values(by= ['conv_rate','num_to_dstn'], ascending= [True, False], inplace= True)
    
   
    
    # Get conv_rates ordered from smallest to largest
    # Exclude 'other' from the conversion rates when calculating quantiles
    conv_rates_no_other= aggs.query('lst_dstn != "other"').sort_values(by=['conv_rate'])
    conv_rates_no_other= conv_rates_no_other['conv_rate'].to_numpy()
    
    # Only create quantile bins if conv_rates_no_other is not empty
    if len(conv_rates_no_other) > 0:
        # split conv_rates_no_other into 20 quantiles
        # Use the bin edges for those quantiles to bin aggs['conv_rate'] which includes 'other'
        bins= np.quantile(conv_rates_no_other, q= np.arange(0,21)/20)
        aggs['lst_dstnLev']= aggs['conv_rate'].transform(lambda x: bisect.bisect_right(bins[:-1], x))
    
    

    # Set lst_dstnLev = 0 where lst_dstn is 'other'
    aggs.loc[aggs['lst_dstn'] == 'other', 'lst_dstnLev']= 0
    
    
    # Sort rows of aggs
    aggs= aggs.sort_values(by= ['lst_dstnLev', 'conv_rate', 'lst_dstn'])

    # filename is created from aggs
    # filename_raw is created from aggs_all
    
    filename= path + file
    filename_raw= filename.replace('.csv','-raw.csv')
    
    # aggs includes the 'other' category
    # agg_all expands out 'other'
    aggs.to_csv(filename, index= False)
    aggs_all.to_csv(filename_raw, index= False)

    
    # add lst_dstnLev column to mydata by
    # left joining aggs on the lst_dstn column
    # .copy() probably unnecessary 
    df_to_merge= aggs[['lst_dstn','lst_dstnLev']].copy()
    
    tmp= mydata.merge(df_to_merge, how='left', on='lst_dstn')

    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe 
    # NOTE: order is changed for tmp, so copy all columns over
    # R code: mydata[, copy(names(tmp)) := tmp]
        
    mydata= tmp


       
    # set NA's in lst_dstnLev to 0
    mydata.fillna(value= {'lst_dstnLev':0}, inplace= True) 

    if print2screen:
        print("These are the lst_dstnLevs:")
        # Concatenate all lst_dstn in the same lst_dstnLev together
        last_destinations= aggs[['lst_dstnLev','lst_dstn']
                               ].groupby( by= 'lst_dstnLev'
                                         ,as_index= False
                                    ).lst_dstn.agg({'last_destination': lambda col: ', '.join(col)})


        # Get Summary statistics for each lst_dstnLev
        # Equivalent to summarize() in R
        lst_dstnLev_stats= aggs.groupby(['lst_dstnLev']
                                         ,as_index= False
                                    ).conv_rate.agg( { 'min':'min'
                                                      ,'1st Quarter': lambda a: np.quantile(a= a, q=0.25)
                                                      ,'median': 'median'
                                                      ,'mean': 'mean'
                                                      ,'3rd Quarter':  lambda a: np.quantile(a= a, q=0.75)
                                                      ,'max':'max'
                                                })

        to_print = pd.merge(lst_dstnLev_stats, last_destinations, on = 'lst_dstnLev')


        display(to_print)

    
    # Drop country_state from mydata
    mydata.drop(columns= ['lst_dstn'], inplace= True)

    # Return aggs
    # Might need to return mydata as well if any changes aren't made inplace
    return aggs, mydata

    

