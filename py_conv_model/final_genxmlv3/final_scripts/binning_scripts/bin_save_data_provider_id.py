def bin_save_data_provider_id(mydata
                              , path = './'
                              , file = 'data-provider-id-level-count.csv'
                              , threshold = 10000
                              , print2screen = True):
    """
    create dplev bin from dataprovider impression counts
    
    small group labeled using @small_group_string
    @small_group_string currently = 'smallDpGroup'
    
    small group conditions
        1:  If count < @threshold 
          OR
        2:  the # of cumulative imps - count for the current dp < @threshold
        
        set dplev = @small_group_string
        otherwise keep the dplev as is
        
    The 2nd condition makes it so that the smallOsGroup has 
    at least @threshold (10k) impressions in it 
    before moving onto the next group level
    
    
    Outputs: 
        - data_providers dataframe
            - columns: ['dataproviderid','count','dplev']
            - order by count, dataproviderid

        -'data-provider-id-level-count.csv' 
            - written from data_providers dataframe
    
    ** May need to return mydata 
    *** If any of the changes are not made inplace
    
    """

    has_columns(mydata, ['target', 'dataproviderid'])
    
    # Name for dp's that don't meet large group criteria
    small_group_string= 'smallDpGroup'
    
    # Create copy aggs_all
    data_providers= mydata[['dataproviderid','target']].copy()
    
    # Group by dataproviderid and 
    # get number of impressions:  count = count
    
    data_providers= data_providers.groupby(by= 'dataproviderid', as_index= False).target.agg({'count':'count'})
    
    # sort by count, dataproviderid
    # both ascending
    
    data_providers.sort_values(by= ['count','dataproviderid'], inplace= True)
    
    # Reset index so it reflects sort order
    data_providers.reset_index(drop= True, inplace= True)
    
    
    # number of data providers above and below @threshold
    # default is 10,000
    num_small_dp= len(data_providers[data_providers['count'] < threshold])
    num_large_dp= len(data_providers[data_providers['count'] >= threshold])
    
    # create initial data provider level column
    data_providers['dplev']= data_providers['dataproviderid'].astype('str')

  
    if num_small_dp == 1:
        # Only add the two smallest data providers to the small group
        # small group name=  @small_group_string
        data_providers.replace({'dplev': {0: small_group_string, 1: small_group_string}}, inplace = True)
        
    elif num_small_dp > 1:
        # create a cumulative sum of impression counts
        # over the sorted count values
        # sorting was done above ( before @num_small_dp and @num_large_dp were calculated ) 
        data_providers['cum_imps'] = data_providers['count'].cumsum()

        # 1:  If count < @threshold 
        #   OR
        # 2:  the # of cumulative imps - count for the current dp < @threshold
        #
        # set dplev = @small_group_string
        # otherwise keep the dplev as is
        
        # 2nd condition makes it so the small group has 
        # a count of 10k impressions
        # before moving onto the next group level
        condition_1= data_providers['count'] < threshold
        condition_2= (data_providers['cum_imps'] - data_providers['count']) < threshold
        
        # df.where replaces all FALSE values with other
        # so setting noth small dp criteria as input :   not (condition 1 or condition 2) 
        ## small dp criteria becomes False and
        ## dplev is set to small_group_string for small dps
        data_providers['dplev'] = data_providers['dplev'].where( ~(condition_1 | condition_2), other= small_group_string)
        
        
        # drop cum_imps column
        data_providers.drop(columns=['cum_imps'], inplace= True)
        

    
    if print2screen:
        print("Small DP group contains: ")
        # all dataproviders where dplev = @small_group_string
        small_data_providers= data_providers[data_providers['dplev'] == small_group_string]['dataproviderid']
        # print small dps series
        display(small_data_providers)

    
    # save data_providers to @file
    # dont't save row numbers
    filename= path + file
    data_providers.to_csv(filename, index= False)
        
    
    # add dplev column to mydata by
    # left joining data_providers on the dataproviderid column
    # .copy() probably unnecessary 
    df_to_merge= data_providers[['dataproviderid','dplev']].copy()
    tmp= mydata.merge(df_to_merge, how='left', on='dataproviderid')
    
    
    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe 
    # NOTE: order is changed for tmp, so copy all columns over
    # R code: mydata[, copy(names(tmp)) := tmp]
        
    mydata= tmp
    
    # set NA's in dplev to 0
    mydata.fillna(value= {'dplev':0}, inplace= True) 


    # Drop dataproviderid from mydata
    mydata.drop(columns= ['dataproviderid'], inplace= True)

    # Return data_providers
    # Might need to return mydata as well if any changes aren't made inplace
    return data_providers, mydata
