def bin_save_domains(mydata, path= './', file= 'conv-rate-level-domain.csv', print2screen = True):
    
    """
    Quantile Bin the domains by conversion rate 
     - only use the @largest_domains to calculate the bin edges
     - 51 bins, 0 to 50
       - level 0 is for all small domains categorized as 'other'
       - Large domains binned between 1 and 50 inclusive ([1,50])
       

    Largest Domains Criteria:
        Number of Impressions >= 3000 
            OR
         Number of Impressions >= 100 AND Conversion Rate >= 0.03
    

    Outputs:
       conv-rate-level-domain.csv
            - conversion rate and impression counts for all domains with 
              'NOWHERE' and non large domains grouped together as 'other'
            
        conv-rate-level-domain-raw.csv
            - conversion rate and impression counts for all ungrouped ("Raw") domains 
       
       
       Returns aggs
           - Dataframe used to create conv-rate-level-domain.csv
           
        ** May need to return mydata as well if any changes are not made inplace

    """
    
    
    # has_columns is a function from adaratools
    # Stop script If has_columns returns False
    assert has_columns(mydata, ['target', 'domain']),"Missing columns in the data.table (target, domain)"
    
    
    
    # Create copy aggs_all
    aggs_all= mydata[['domain','target']].copy()
    
    # Group by country_state and for each
    # get conversion rate:        conv_rate= mean(target)
    # get number of impressions:  num_from_domain = count
    aggs_all= aggs_all.groupby(by= 'domain', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                           ,'num_from_domain':'count'
                                                                            })
    # Sort by: 
    #    conv_rate: ascending
    #    num_from_domain: descending
    aggs_all.sort_values(by= ['conv_rate','num_from_domain'], ascending= [True, False], inplace= True)
    
    
    # get a list of the largest domains
    largest_domains= aggs_all.query('(num_from_domain >= 3000) | ((num_from_domain >= 100) & (conv_rate >= 0.03))'
                                     )['domain'].array
    


    # assign country_state to 'other' for 
    # all domains not in largest_domains 
    mydata.loc[~mydata['domain'].isin(largest_domains), 'domain']= 'other' 
    
    
    # Create copy aggs
    # Different from aggs_all becasue
    # All domains NOT in largest_domains have been set to 'other'
    aggs= mydata[['domain','target']].copy()
    
    
    # Group by new set of country_state's and for each
    # get conversion rate:        conv_rate= mean(target)
    # get number of impressions:  num_from_domain = count
    
    aggs= aggs.groupby(by= 'domain', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                   ,'num_from_domain':'count'
                                                            })
    # Sort by 
    #    conv_rate: ascending 
    #    num_from_domain: descending
    
    aggs.sort_values(by= ['conv_rate','num_from_domain'], ascending= [True, False], inplace= True)

    
        
    # Get conv_rates ordered from smallest to largest
    # Exclude 'other' from the conversion rates when calculating quantiles
    conv_rates_no_other= aggs.query('domain != "other"').sort_values(by=['conv_rate'])
    conv_rates_no_other= conv_rates_no_other['conv_rate'].to_numpy()
    
    
    # Only create quantile bins if conv_rates_no_other is not empty
    if len(conv_rates_no_other) > 0:
        # split conv_rates_no_other into 50 quantiles
        # Use the bin edges for those quantiles to bin aggs['conv_rate'] which includes 'other'
        bins= np.quantile(conv_rates_no_other, q= np.arange(0,51)/50)
        aggs['domainlev']= aggs['conv_rate'].transform(lambda x: bisect.bisect_right(bins[:-1], x))
    
        
    # Set domainlev = 0 where country_state is 'other'
    aggs.loc[aggs['domain'] == 'other', 'domainlev']= 0
    
    # Sort rows of aggs
    aggs= aggs.sort_values(by= ['domainlev', 'conv_rate', 'domain'])
    
    

    
    # filename is created from aggs
    # filename_raw is created from aggs_all
    filename= path + file
    filename_raw= filename.replace('.csv','-raw.csv')
    
    # aggs includes the 'other' category
    # agg_all expands out 'other'
    # Save both to files
    # Don't save the index/row number

    aggs.to_csv(filename, index= False)
    aggs_all.to_csv(filename_raw, index= False)
    
    
    
    # add country_statelev column to mydata by
    # left joining aggs on the country_state column
    # .copy() probably unnecessary 
    df_to_merge= aggs[['domain','domainlev']].copy()
    tmp= mydata.merge(df_to_merge, how='left', on='domain')

    
    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe  
    # if I want to preserve column ordering i can just save the column names from before then reorder using them
    # R code: mydata[, copy(names(tmp)) := tmp]
    mydata= tmp

    
    # set Nulls in country_statelev to 0
    mydata.fillna(value= {'domainlev':0}, inplace= True) 

    
    if print2screen:
        # Print out the domain levels
        print("These are the domainlevs:")

        # Concatenate all country_states in the same country_statelev together
        domains= aggs.groupby(  by= ['domainlev']
                                      ,as_index= False
                                ).domain.agg({'domains': lambda col: ', '.join(col)})

        # Get Summary statistics for each country_statelev
        # Equivalent to summarize() in R
        domainlev_stats= aggs.groupby( by= 'domainlev'
                                       ,as_index= False
                                    ).conv_rate.agg({  'min':'min'
                                                        ,'1st Quarter': lambda a: np.quantile(a= a, q=0.25)
                                                        ,'median': 'median'
                                                        ,'mean': 'mean'
                                                        ,'3rd Quarter':  lambda a: np.quantile(a= a, q=0.75)
                                                        ,'max':'max'
                                                })


        to_print = pd.merge(domainlev_stats, domains, on = 'domainlev')

        display(to_print)

    # Drop country_state from mydata
    mydata.drop(columns= ['domain'], inplace= True)

    # Return aggs
    # Might need to return mydata as well if any changes aren't made inplace
    return aggs, mydata
    