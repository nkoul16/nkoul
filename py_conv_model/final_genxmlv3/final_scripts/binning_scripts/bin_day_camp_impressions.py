def bin_day_camp_impressions(mydata):
    """
    Bin daycampaignimpressions column 
    drop prebinned column day_campaign_impressions_bin if exists
    
    mydata is modified inplace
    
    Returns None
    """
    mydata.drop(columns=['day_campaign_impressions_bin'], errors='ignore', inplace=True)
    
    bins=[0,1,2,5,999999999]
    labels=['0-1', '2', '3-5', '6-999999999']
    mydata['daycampaignimpressions'] = pd.cut(  mydata['daycampaignimpressions']
                                                , bins= bins
                                                , labels = labels
                                                , include_lowest= True
                                              )