def bin_save_browser(mydata, path='./', file='browser-level-count.csv', threshold= 10000, print2screen= True):
    """
    create browserlev bin from browser impression counts
    
    small group labeled using @small_group_string
    @small_group_string currently = 'smallBrowserGroup'
    
    small group conditions
        1:  If count < @threshold 
          OR
        2:  the # of cumulative imps - count for the current browser < @threshold
        
        set browserlev = @small_group_string
        otherwise keep the browserlev as is
        
    The 2nd condition makes it so that the smallBrowserGroup has 
    at least @threshold (10k) impressions in it 
    before moving onto the next group level
    
    
    Outputs: 
        - browsers dataframe
            - columns: ['browser','count','browserlev']
            - order by count, browser

        -'browser-level-count.csv' 
            - written from browsers dataframe
    
    ** May need to return mydata 
    *** If any of the changes are not made inplace
    
    """
    
    
    # check to make sure DataFrame the columns below
    has_columns(mydata, ['target', 'browser'])
    

    # Name for browser's that don't meet large group criteria
    small_group_string= 'smallBrowserGroup'
    
    # Create copy browsers
    browsers= mydata[['browser','target']].copy()
    
    
    # Group by browser and 
    # get number of impressions:  count = count
    
    browsers= browsers.groupby(by= 'browser', as_index= False).target.agg({'count':'count'})
    
    # sort by count, browser
    # both ascending
    
    browsers.sort_values(by= ['count','browser'], inplace= True)
    
    # Reset index so it reflects sort order
    browsers.reset_index(drop= True, inplace= True)
    
  

    # number of data providers above and below @threshold
    # default is 10,000
    num_small_br= len(browsers[browsers['count'] < threshold])
    num_large_br= len(browsers[browsers['count'] >= threshold])
    

    
    # create initial browser level column
    browsers['browserlev']= browsers['browser'].astype('str')


    if num_small_br == 1:
        # Only add the two smallest browsers to the small group
        # small group name=  @small_group_string
        browsers.replace({'browserlev': {0: small_group_string, 1: small_group_string}}, inplace = True)
    
    elif num_small_br > 1:
        # create a cumulative sum of impression counts
        # over the sorted count values
        # sorting was done above ( before @num_small_br and @num_large_br were calculated ) 
        browsers['cum_imps'] = browsers['count'].cumsum()

        # 1:  If count < @threshold 
        #   OR
        # 2:  the # of cumulative imps - count for the current browser < @threshold
        #
        # set browserlev = @small_group_string
        # otherwise keep the browserlev as is
        
        ## 2nd condition makes it so that the next browsers can only be @threshold - 1 smaller  
        ## than the sum of all previous levels to be considered a new level
        ## Otherwise the browser is considered and grouped as small 
        condition_1= browsers['count'] < threshold
        condition_2= (browsers['cum_imps'] - browsers['count']) < threshold
        
        # df.where replaces all FALSE values with other
        # so setting not small browser criteria as input :   not (condition 1 or condition 2) 
        ## small browser criteria becomes False and
        ## browserlev is set to small_group_string for small browsers
        browsers['browserlev'] = browsers['browserlev'].where( ~(condition_1 | condition_2), other= small_group_string)
        
        
        # drop cum_imps column
        browsers.drop(columns=['cum_imps'], inplace= True)
        

    

    if print2screen:
        print("Small browser group contains: ")
        # all browsers where browserlev = @small_group_string
        small_browsers= browsers[browsers['browserlev'] == small_group_string]['browser']
        # print small_browsers series
        display(small_browsers)
        
    

    # save data_providers to @file
    # dont't save row numbers
    filename= path + file
    browsers.to_csv(filename, index= False)
    
    
    # add browserlev column to mydata by
    # left joining data_providers on the dataproviderid column
    # .copy() probably unnecessary 
    df_to_merge= browsers[['browser','browserlev']].copy()
    tmp= mydata.merge(df_to_merge, how='left', on='browser')
    
    
    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe 
    # NOTE: order is changed for tmp, so copy all columns over
    # R code: mydata[, copy(names(tmp)) := tmp]
        
    mydata= tmp
    
    # set NA's in browserlev to 0
    mydata.fillna(value= {'browserlev':0}, inplace= True) 


    # Drop dataproviderid from mydata
    mydata.drop(columns= ['browser'], inplace= True)

    # Return browsers
    # Might need to return mydata as well if any changes aren't made inplace
    return browsers, mydata
    


