def bin_hour_of_day(mydata):
    """
    bins hourofday column 
    modifies input df @mydata inplace
    
    """
    
    
    has_columns(mydata, ['target', 'hourofday'])
    
    bins=[0, 2, 5, 8, 11, 14, 17, 20, 23]
    labels=['0-2', '3-5', '6-8', '9-11', '12-14', '15-17', '18-20', '21-23']
    
    mydata['hourofday'] = pd.cut(  mydata['hourofday']
                                   ,bins= bins
                                   ,labels = labels
                                   ,include_lowest= True
                                )