def bin_save_country_state_conv_rate_qtile(mydata, path= './', file= 'conv-rate-level-country_state.csv', print2screen= True):
    """
    Create country_statelev bin from country and state columns
    quantile bin
    quantiles used: 0 to 1 by intervals of 0.02 (np.arange(0,0.02,1.02) or np.arange(0,51)/50 )
    quantiles calculated over the conversion rates of the largest locations
    
    largest locations criteria:
    
    ( #_imps_at_location >= 5000 )  OR  ( #__imps_at_location >= 100 & conv_rate >= 0.03 )
    
    All smaller locations set to 'other'
    
    country_statelev for 'other' and all NA's set to 0
    
    Returns aggs: the conversion_rate information of the largest locations 
                  with all of the small locations grouped as other
                  
            May need to return mydata if any of the changes are not made inplace
    """
    
    
    # has_columns is a function from adaratools
    has_columns(mydata, ['target', 'country', 'state'])
    
    # combine country and state into one column 
    # Drop individual country and state columns
    mydata['country_state']= mydata['country'].str.cat(mydata['state'], sep= '_')
    mydata.drop(columns=['country','state'], inplace= True)
    
    # Create copy aggs_all
    aggs_all= mydata[['country_state','target']].copy()
    
    # Group by country_state and for each
    # get conversion rate:        conv_rate= mean(target)
    # get number of impressions:  num_at_location = count
    aggs_all= aggs_all.groupby(by= 'country_state', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                                 ,'num_at_location':'count'
                                                                            })
    # Sort by: 
    #    conv_rate: descending
    #    num_at_location: descending
    aggs_all.sort_values(by= ['conv_rate','num_at_location'], ascending= [False, False], inplace= True)
    
    
    # get a list of the largest locations
    largest_locations= aggs_all.query('(num_at_location >= 5000) | ((num_at_location >= 100) & (conv_rate >= 0.03))'
                                     )['country_state'].array
    
    

    # assign country_state to 'other' for 
    # all locations not in largest_locations 
    mydata.loc[~mydata['country_state'].isin(largest_locations), 'country_state']= 'other' 
    
    
    # Create copy aggs
    # Different from aggs_all becasue
    # All locations not in largest_locations have been set to 'other'
    aggs= mydata[['country_state','target']].copy()
    
    
    # Group by new set of country_state's and for each
    # get conversion rate:        conv_rate= mean(target)
    # get number of impressions:  num_at_location = count
    
    aggs= aggs.groupby(by= 'country_state', as_index= False).target.agg({ 'conv_rate':'mean'
                                                                         ,'num_at_location':'count'
                                                                    })
    # Sort by 
    #    conv_rate: ascending 
    #    num_at_location: descending
    
    aggs.sort_values(by= ['conv_rate','num_at_location'], ascending= [True, False], inplace= True)

    
        
    # Get conv_rates ordered from smallest to largest
    # Exclude 'other' from the conversion rates when calculating quantiles
    conv_rates_no_other= aggs.query('country_state != "other"').sort_values(by=['conv_rate'])
    conv_rates_no_other= conv_rates_no_other['conv_rate'].to_numpy()
    
    
    # split conv_rates_no_other into 50 quantiles
    # Use the bin edges for those quantiles to bin aggs['conv_rate'] which includes 'other'
    bins= np.quantile(conv_rates_no_other, q= np.arange(0,51)/50)
    aggs['country_statelev']= aggs['conv_rate'].transform(lambda x: bisect.bisect_right(bins[:-1], x))
    

    
    # Set country_statelev = 0 where country_state is 'other'
    aggs.loc[aggs['country_state'] == 'other', 'country_statelev']= 0
    
    # Sort rows of aggs
    aggs= aggs.sort_values(by= ['country_statelev', 'conv_rate', 'country_state'])
    
    # Save aggs to csv: 'conv-rate-level-country_state.csv'
    # Don't save the index/row number
    filename= path + file
    aggs.to_csv(filename, index= False)
    

    
    # add country_statelev column to mydata by
    # left joining aggs on the country_state column
    # .copy() probably unnecessary 
    df_to_merge= aggs[['country_state','country_statelev']].copy()
    tmp= mydata.merge(df_to_merge, how='left', on='country_state')
    
    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe  
    # if I want to preserve column ordering i can just save the column names from before then reorder using them
    # R code: mydata[, copy(names(tmp)) := tmp]
    mydata= tmp

    # set Nulls in country_statelev to 0
    mydata.fillna(value= {'country_statelev':0}, inplace= True) 

    if print2screen:
        # Print out the country_state levels
        print("These are country_statelevs:")

        # Concatenate all country_states in the same country_statelev together
        country_states= aggs.groupby(  by= ['country_statelev']
                                      ,as_index= False
                                ).country_state.agg({'country_states': lambda col: ', '.join(col)})

        # Get Summary statistics for each country_statelev
        # Equivalent to summarize() in R
        country_statelev_stats= aggs.groupby( by= 'country_statelev'
                                             ,as_index= False
                                        ).conv_rate.agg({  'min':'min'
                                                          ,'1st Quarter': lambda a: np.quantile(a= a, q=0.25)
                                                          ,'median': 'median'
                                                          ,'mean': 'mean'
                                                          ,'3rd Quarter':  lambda a: np.quantile(a= a, q=0.75)
                                                          ,'max':'max'
                                                    })


        to_print = pd.merge(country_statelev_stats, country_states, on = 'country_statelev')

        display(to_print)

    # Drop country_state from mydata
    mydata.drop(columns= ['country_state'], inplace= True)

    # Return aggs
    # Might need to return mydata as well if any changes aren't made inplace
    return aggs, mydata
    

    
    



    
