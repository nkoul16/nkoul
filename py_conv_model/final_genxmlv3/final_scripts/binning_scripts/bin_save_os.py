
def bin_save_os(mydata, path='./', file='os-level-count.csv', threshold= 10000, print2screen= True):
    """
    create oslev bin from os impression counts
    
    small group labeled using @small_group_string
    @small_group_string currently = 'smallOsGroup'
    
    small group conditions
        1:  If count < @threshold 
          OR
        2:  the # of cumulative imps - count for the current os < @threshold
        
        set oslev = @small_group_string
        otherwise keep the oslev as is
        
    The 2nd condition makes it so that the smallOsGroup has 
    at least @threshold (10k) impressions in it 
    before moving onto the next group level
    
    
    Outputs: 
        - oss dataframe
            - columns: ['os','count','oslev']
            - order by count, os

        -'os-level-count.csv' 
            - written from oss dataframe
    
    ** May need to return mydata 
    *** If any of the changes are not made inplace
    
    """
    
    
    # check to make sure DataFrame the columns below
    has_columns(mydata, ['target', 'os'])
    

    # Name for os's that don't meet large group criteria
    small_group_string= 'smallOsGroup'
    
    # Create copy oss
    oss= mydata[['os','target']].copy()
    
    
    # Group by os and 
    # get number of impressions:  count = count
    
    oss= oss.groupby(by= 'os', as_index= False).target.agg({'count':'count'})
    
    # sort by count, os
    # both ascending
    
    oss.sort_values(by= ['count','os'], inplace= True)
    
    # Reset index so it reflects sort order
    oss.reset_index(drop= True, inplace= True)
    
  

    # number of data providers above and below @threshold
    # default is 10,000
    num_small_os= len(oss[oss['count'] < threshold])
    num_large_os= len(oss[oss['count'] >= threshold])
    

    
    # create initial os level column
    oss['oslev']= oss['os'].astype('str')


    if num_small_os == 1:
        # Only add the two smallest oss to the small group
        # small group name=  @small_group_string
        oss.replace({'oslev': {0: small_group_string, 1: small_group_string}}, inplace = True)
    
    elif num_small_os > 1:
        # create a cumulative sum of impression counts
        # over the sorted count values
        # sorting was done above ( before @num_small_os and @num_large_os were calculated ) 
        oss['cum_imps'] = oss['count'].cumsum()

        # 1:  If count < @threshold 
        #   OR
        # 2:  the # of cumulative imps - count for the current os < @threshold
        #
        # set oslev = @small_group_string
        # otherwise keep the oslev as is
        
        ## 2nd condition makes it so the small group has a count of 10k impressions
        # before moving onto the next group level
        condition_1= oss['count'] < threshold
        condition_2= (oss['cum_imps'] - oss['count']) < threshold
        
        # df.where replaces all FALSE values with other
        # so setting noth small os criteria as input :   not (condition 1 or condition 2) 
        ## small os criteria becomes False and
        ## oslev is set to small_group_string for small oss
        oss['oslev'] = oss['oslev'].where( ~(condition_1 | condition_2), other= small_group_string)
        
        
        # drop cum_imps column
        oss.drop(columns=['cum_imps'], inplace= True)
        

    

    if print2screen:
        print("Small os group contains: ")
        # all oss where oslev = @small_group_string
        small_oss= oss[oss['oslev'] == small_group_string]['os']
        # print small_oss series
        display(small_oss)
        
    

    # save data_providers to @file
    # dont't save row numbers
    filename= path + file
    oss.to_csv(filename, index= False)
    
    
    # add oslev column to mydata by
    # left joining data_providers on the dataproviderid column
    # .copy() probably unnecessary 
    df_to_merge= oss[['os','oslev']].copy()
    tmp= mydata.merge(df_to_merge, how='left', on='os')
    
    
    # Copy tmp to mydata
    # do I need to use copy? from Zheng's note, maybe 
    # NOTE: order is changed for tmp, so copy all columns over
    # R code: mydata[, copy(names(tmp)) := tmp]
        
    mydata= tmp
    
    # set NA's in oslev to 0
    mydata.fillna(value= {'oslev':0}, inplace= True) 


    # Drop os from mydata
    mydata.drop(columns= ['os'], inplace= True)

    # Return oss
    # Might need to return mydata as well if any changes aren't made inplace
    return oss, mydata
    


