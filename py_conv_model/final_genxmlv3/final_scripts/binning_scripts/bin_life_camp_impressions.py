def bin_life_camp_impressions(mydata):
    """
    Bin lifetimecampaignimpressions column 
    
    drop prebinned column day_campaign_impressions_bin if exists
    
    mydata is modified inplace
    
    Returns None
    """
    mydata.drop(columns=['lifetime_campaign_impressions_bin'], errors='ignore',inplace=True)
    
    bins=[0,3,10,20,999999999]
    labels=['0-3', '4-10', '11-20', '21-999999999']
    mydata['lifetimecampaignimpressions'] = pd.cut(   mydata['lifetimecampaignimpressions']
                                                      , bins= bins
                                                      , labels = labels
                                                      , include_lowest= True
                                                    )
