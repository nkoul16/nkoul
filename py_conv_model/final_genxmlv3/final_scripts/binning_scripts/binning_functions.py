def create_bin_from_df_col(mydata, col_name='', bins=[], labels=[], include_lowest= True):
    """
    Template binning function for pandas DataFrame
    @col_name is the column you want to bin
    @include_lowest determines wheter or not to make the interval fully inclusive

        include_lowest= True : [0,1] [1,2]
        include_lowest= False: (0,1] (1,2]

    operates on dataframe inplace
    """
    
    
    if include_lowest:
        mydata[col_name] = pd.cut( mydata[col_name]
                                   ,bins= bins
                                   ,labels = labels
                                   ,include_lowest= True
                                   ,duplicates= 'drop'
                                  )
    else:
        mydata[col_name] = pd.cut( mydata[col_name]
                                   ,bins= bins
                                   ,labels = labels
                                   ,include_lowest= False
                                   ,duplicates= 'drop'
                                  )

def bin_counts(mydata, source_col, target_col):
    """
    default binning for bin_type == 'count'
    """

    bins = [-0.01, 0, 1, 2, 3, 999999999]
    labels = ['0', '1', '2', '3', '4-999999999']

    # Call Template Binning function 
    # This will bin the source_col in mydata inplace
    create_bin_from_df_col(mydata, source_col, bins, labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)


def bin_recency_by_qtile(mydata, source_col, target_col):
    """
    adds 5 bins onto the @default_bins below
    5 extra bins are created by splitting the @large_values 
    of the column into 5 quantiles

    these custom bins and their labels are then added to 
    their respective default arrays

    default binning process for bin_type == 'quantile'

    """

    # The default bin edges and labels
    # Custom bin edges and labels will be added onto both
    default_labels= ['0', '1', '2', '3-4']
    default_bins=[-0.01 , 0, 1, 2, 4]


    # get all large values of mydata[source_col] 
    large_values= mydata[(mydata[source_col] >= 5) & (mydata[source_col] < 999999999)][source_col].to_numpy()



    # Create custom_bins

    # bin @large_values into 5 quantiles
    # round all bin edges up
    # get all unique bin edges
    # these bin edges will be used to make the final bins
    bin_cuts = np.unique(np.ceil(np.quantile(large_values,np.arange(0, 1.2, 1/5))))

    # Create Custom labels
    # default bins/labels end at 4 so
    # custom bins start at 5 and end at 999999999

    # left and right bin edges
    left_edge = np.r_[bin_cuts]
    right_edge = np.r_[bin_cuts[1:] - 1,999999999]
    

    # create custom labels
    # string concat left and right edge arrays with '_' separtor 
    custom_labels= [ str(int(i)) + "-" + str(int(j)) for i,j in zip(left_edge,right_edge) ]

    # Create final bins and labels

    # add the right_edges to the default bins
    final_bins= np.r_[default_bins, right_edge]

    # add custom labels to default_labels
    final_labels= np.r_[default_labels, custom_labels]


    # Call Genral Binning function 
    create_bin_from_df_col(mydata, source_col, bins= final_bins, labels= final_labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)



def bin_recency_by_day(mydata, source_col, target_col):
    """
    default binning for bin_type == 'day'
    """

    bins = [-999999999, -1, 3, 7, 14, 30, 60, 999999999]
    labels = ['-1', '0-3', '4-7', '8-14', '15-30', '31-60', '61-999999999']

    # Call Template Binning function 
    # This will bin the source_col in mydata inplace
    create_bin_from_df_col(mydata, source_col, bins, labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)



def bin_recency_by_180_days(mydata, source_col, target_col):
    """
    default binning for bin_type == 'day180'
    DIFFERENT from bin_recency_by_day 
    2 largest bins are different

    bin_recency_by_day:          31-60 and 61-999999999  
    bin_recency_by_180_days:     31-180 and 181-999999999 
    """

    bins = [0, 3, 7, 14, 30, 180, 999999999]
    labels = ['0-3', '4-7', '8-14', '15-30', '31-180', '181-999999999']


    # Call Template Binning function 
    # This will bin the source_col in mydata inplace
    create_bin_from_df_col(mydata, source_col, bins, labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)



def bin_recency_by_minute(mydata, source_col, target_col):
    """
    default binning for bin_type == 'minute'
    """
    bins = [0, 1, 10, 60, 720, 1400, 999999999]
    labels = ['0-1', '2-10', '11-60', '61-720', '721-1400', '1401-999999999']

    # Call Template Binning function 
    # This will bin the source_col in mydata inplace
    create_bin_from_df_col(mydata, source_col, bins, labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)



def bin_recency_by_hour(mydata, source_col, target_col):
    """
    default binning for bin_type == 'hour'
    """
    bins = [0, 6, 24, 96, 168, 720, 1440, 999999999]
    labels = ['0-6', '7-24', '25-96', '97-168', '169-720', '721-1440', '1441-999999999']

    # Call Template Binning function 
    # This will bin the source_col in mydata inplace
    create_bin_from_df_col(mydata, source_col, bins, labels)

    # Rename Columns inplace
    mydata.rename(index=str, columns={source_col: target_col}, inplace = True)

    
    
