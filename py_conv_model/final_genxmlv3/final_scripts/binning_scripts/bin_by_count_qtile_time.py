def bin_by_count_qtile_time(mydata):
    """
    Bins the remaining columns in mydata based 
    on their listed bin type 
    
    bin_types are found in csv's that are read into 
    dataframe @xref_df by get_variable_bin_types() function
    
    for variables not in @xref_df bins type is determined
    by matching a pattern associated with a bin type to 
    the variable name in @mydata not in @xref_df
    
    @mydata is operated on inplace
    return None
    """
    
    # get bin_types for all vairables
    # create binnned_name by adding 'bin_' prefix to all variable names
    xref_df= get_variable_bin_types()
    xref_df['binned_name'] = xref_df['var_name'].radd('bin_')
    
    # All columns in data_df
    cols_in_data = sorted(mydata.columns)

    # get variables from xref_df in @cols_in_data by bin type
    
    # default condition
    # variable is in @mydata 
    default= xref_df['var_name'].isin(cols_in_data)
    
    # case conditions list
    # bin_type conditions for each variable
    cond_case_1= xref_df['bin_type'] == 'count'
    cond_case_2= xref_df['bin_type'] == 'quantile'
    cond_case_3= xref_df['bin_type'] == 'day'
    cond_case_4= xref_df['bin_type'] == 'day180'
    cond_case_5= xref_df['bin_type'] == 'minute'
    cond_case_6= xref_df['bin_type'] == 'hour'
    

    
    # Filter xref_df using above conditions
    
    # Case 1
    df_counts = xref_df[default & cond_case_1]
    
    # Case 2
    df_recency_quantile = xref_df[default & cond_case_2]
    
    # Case 3
    df_recency_day = xref_df[default & cond_case_3]
    
    # Case 4
    df_recency_180_day = xref_df[default & cond_case_4]
    
    # Case 5
    df_recency_minute = xref_df[default & cond_case_5]
    
    # Case 6
    df_recency_hour = xref_df[default & cond_case_6]
    
    
    
    def bin_cases(mydata, xref_df, bin_function, col_list= False):
        """
        Creates a bin for each variable in the df passed in
        by using the @bin_function passed in
        
        If @col_list= True 
        then you can input a list/array into xref_df instead 
        of pandas.DataFrame
        
        """
        n = len(xref_df)
        if n > 0:
            if col_list:
                for col_name in xref_df:
                    old_name = col_name
                    new_name = 'bin_' + col_name
                    bin_function(mydata, old_name, new_name)
                
            else:
                for i in range(n):
                    row = xref_df.iloc[i]
                    old_name = row['var_name']
                    new_name = row['binned_name']
                    bin_function(mydata, old_name, new_name)
                
                
    # The bin_functions called were created as separate functions
    # bin_functions used in bin_cases modify @mydata in place
    bin_cases(mydata, xref_df= df_counts            ,bin_function= bin_counts)
    bin_cases(mydata, xref_df= df_recency_quantile  ,bin_function= bin_recency_by_qtile)
    bin_cases(mydata, xref_df= df_recency_day       ,bin_function= bin_recency_by_day)
    bin_cases(mydata, xref_df= df_recency_180_day   ,bin_function= bin_recency_by_180_days)
    bin_cases(mydata, xref_df= df_recency_minute    ,bin_function= bin_recency_by_minute)
    bin_cases(mydata, xref_df= df_recency_hour      ,bin_function= bin_recency_by_hour)

        
    
    # Edge Cases
    # Where variable in @cols_in_data not in xref_df['var_name']
    edge_case_cols= [ i for i in cols_in_data if i not in xref_df['var_name'].tolist() ]
    
    
    # Determine how to bin columns in @edge_case_cols
    # by matching string column name ends with
    def edge_case_bins(mydata, edge_case_cols, pattern, bin_function):
        """
        if pattern matches any cols in @edge_case_cols
        bin the matched columns
        """
        edge_cols= [i for i in edge_case_cols if i.endswith(pattern)]
        
        if len(edge_cols) > 0:
            bin_cases(mydata, xref_df= edge_cols, bin_function= bin_function, col_list= True)
            
    
    # New Frequency Columns 
    edge_case_bins(mydata, edge_case_cols, pattern= '_freq', bin_function= bin_counts)
    # New Hour Columns
    edge_case_bins(mydata, edge_case_cols, pattern= '_rcy_by_hour', bin_function= bin_recency_by_hour)
    # New Day Columns
    edge_case_bins(mydata, edge_case_cols, pattern= '_rcy_by_day', bin_function= bin_recency_by_day)

    

    
    

