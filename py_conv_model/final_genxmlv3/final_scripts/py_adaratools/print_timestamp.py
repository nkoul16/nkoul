def print_timestamp(word):
    """
    Prints the datetime in yyyy/mm/dd hh:mm:ss format 
    followed by '-- ' + @word 
    
    """
    time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print(time + " -- " + word)

    