def df_strip(df, inplace= True):
    """
    Strips whitespace from all dtype: object columns 
        (Pandas stores strings as object dtypes) 
        
    Strips all column name and only object dtype values
    
    If inplace= False a copy will be returned
    """
    if not inplace:
        df = df.copy()
        
    # Get list of string/object columns
    string_columns= df.select_dtypes(include=['object']).columns    
        
    # Remove Whitespace
    for col in string_columns:
        df[col] = df[col].str.strip()
        
    
    df.columns= df.columns.str.strip()     
     
    if not inplace: return df
    
