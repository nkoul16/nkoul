def has_columns(mydata, cols2check, emptyDfOk = True):
    """
    Checks to see if columns @cols2check is in the DataFrame mydata
    if emptyDfOk == True then it is ok for the dataframe to be empty 
       AND default is to assume @cols2check are in the Dataframe 
                           WITHOUT checking
                           
    Returns True of False
    If False prints warning message 
        ** should probably use logging or warning module instead of print
    """
    if len(mydata) == 0 & emptyDfOk: 
        return True

    elif len(mydata) == 0:
        msg= ', '.join(cols2check)
        print('checking an empty data frame for: {}'.format(msg))
        return False
    
    else:
        missing_columns = np.setdiff1d(cols2check, mydata.columns)
        if len(missing_columns) > 0:
            msg= ', '.join(missing_columns)
            print('data missing. the following columns are missing: {}'.format(msg))
            
            return False
        
        else:
            return True