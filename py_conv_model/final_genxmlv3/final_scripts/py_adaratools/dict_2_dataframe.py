def dict_2_dataframe(data_dict, name=''):
    key_col=[]
    name_col=[]
    value_col=[]
    for key,value in data_dict.items():
        for i in value:
            name_col.append(name)
            key_col.append(key)
            value_col.append(i)
            
    
    data= { 'name': name_col
           ,'key': key_col
           ,'value': value_col
          }
    
    df= pd.DataFrame(data)
    
    return df