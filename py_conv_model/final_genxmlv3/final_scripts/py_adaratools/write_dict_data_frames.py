# write_list_data_frames.R
def write_dict_data_frames(dct, file, header):
    """
    takes in a dictionary of dataframes @dct
    and writes them all into 1 file
    
    @header is written as the 1st line in the file
    
    this will overwrite a file if it currently exists
    """
    
    with open(file, 'w') as f:
        f.write(header + '\n\n')
    
    for col_name, df in dct.items():
        df.to_csv(file, index= False, sep= '\t', mode= 'a')
            
        with open(file, 'a') as f:
            f.write('\n')

        