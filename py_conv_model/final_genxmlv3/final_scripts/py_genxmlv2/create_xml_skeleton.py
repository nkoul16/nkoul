# create_xml_skeleton.R

#' Generate an empty model template xml
#' 
#' @param camp_id the campaign id (default = 'REPLACE_WITH_CAMPAIGN_ID')
#' @param opt_pol optimization_policy 'C' or 'V' (click or conversion) 
#' (default = 'V')
#' @param template_name what to call this template
#' @return a bare-bones model template xml
#' @export

import lxml.etree as ET
from lxml.builder import E

def create_xml_skeleton(camp_id = 'REPLACE_WITH_CAMPAIGN_ID'
                        , opt_pol = 'V'
                        , template_name = 'REPLACE_WITH_TEMPLATE_NAME'):
    """
    Creates the XML Skeleton using lxml
         import lxml.etree as ET
         from lxml.builder import E
    
    E.p(version='2.0')
    creates a new node with tag 'p' and attribute version='2.0'
    ie: <p version="2.0">
    
    ET.Comment('text') creates a xml comment
    eg: ET.Comment('This is a model template XML file for:')
           returns:
           
           <!--This is a model template XML file for:-->

    
    You can create child(nested) nodes by 
        using the .append() method on a node
            or
        creating aa new node within a node using E from lxml.builder
        
        eg:     transform_node= (
                            E.transform(E.variables(),
                            type='simple'
                           )
                        )
                returns:
                          <transform type="simple">
                            <variables/>
                          </transform>
    

    Returns: @parent - a XML Tree Seleton
    """

    
    # Parent Node
    parent= E.p(version='2.0')
    
    # Child Nodes
    id_node= E.id(camp_id + '1')
    mod_name_comment_node= ET.Comment('This is a model template XML file for: ' + camp_id)
    train_node= E.trainingId('0')
    camp_id_node= E.campaignId(camp_id)
    policy_node= E.policy(opt_pol)
    properties_node = ( 
        E.properties( 
            E.template(
                E.id('REPLACE_WITH_MODEL_TEMPLATE_ID'),
                E.name(template_name)
            ),
        E.campaign(
            E.id(camp_id),
            E.name('FILL IN WITH CAMPAIGN NAME')
            )
        )
    )

    transform_node= (
        E.transform(E.variables(),
                    type='simple'
                   )
    )

    prediction_node= (
        E.prediction(E.algorithm(),
                    type='logit'
                   )
    )
    percentile_node= E.percentile()


    # Append Children to Parents
    parent.append(id_node)
    parent.append(mod_name_comment_node)
    parent.append(train_node)
    parent.append(camp_id_node)
    parent.append(policy_node)
    # TODO add the properties node to the template xml - check with Menon
    parent.append(properties_node)
    parent.append(transform_node)
    parent.append(prediction_node)
    parent.append(percentile_node)
    
    return parent

