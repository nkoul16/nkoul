# get_build_param.R

def get_build_param(file = 'build_param.csv', path = './', domain_key_id = 'domain'):
    """
    Creates DataFrames from  build_param.csv
                            , product-variable-bin-type-source.csv
                            , category-keys.csv **


    Also creates another DataFrame from some hard coded values
    
    Modifies and combines all of these DataFrames 
    to get the necessary information for creating 
    XML variable-set parent nodes from them 


    Returns: 1 DataFrame containing XML variable-set parent node information
             
             3 columns:    xml_vset_var     -- XML variable-set name
                         , xml_vset_src     -- XML source_set
                         , default_val_type -- XML variable default_value type
                                                     used to determine XML default_value
                                                -- (e.g. count, recency, loyalty, etc) 
                                 
                         
    Example XML variable-set parent node:

    <variable-set name="adunitid_vset" source_set="adUnitId" default_variable="adunitid_default" default_value="1">
    

    ** Gets called in @get_category_key_id() function (from modetrain package)
       For build_param.csv files with product_id as a parameter name
       So ONLY for Click Models
       and MAYBE General Product Vertical Models (GPVM) 
    
    """
    

    # Read in Build paramaters to dataframe
    file = path + file
    df = pd.read_csv(file, header= 0, sep='|', )
    df_strip(df)
    df.columns = df.columns.str.lower()

    # Create new columns default_val_type
    # will be used to create XML file
    df['default_val_type'] = np.nan
    df.loc[df['value'].str.contains('keytsday_'), 'default_val_type'] = 'recency'
    df.loc[df['value'].str.contains('ckv_'), 'default_val_type'] = 'count'
  
    # copy the build parameter df before modifying
    # this is becasue you need it in case product_id is one of the
    # input parameters you will need to call it again down below
    # honestly if product_id is NULL it shouldn't matter
    # Could also just move that portion up here and modify it all at once
    var_src_build_params= df.copy()
    var_src_build_params.dropna(axis = 0, subset=['default_val_type'], inplace= True)
    
    # Create xml compatible variable names in var_src_build_params 
    var_src_build_params['parameter_name'] = var_src_build_params['parameter_name'].str.replace('_ckv_key_id','_conversion_count')
    var_src_build_params['new_param_name'] = var_src_build_params['parameter_name'].str.replace('_keyts_key_id','_conversion_rcy')
    var_src_build_params['new_param_name'] = var_src_build_params['parameter_name'].str.replace('conversion_advtsr_all','conversion_count')
    var_src_build_params['bin_param_name'] = var_src_build_params['new_param_name'].radd('bin_')
    
    
    # Get XML components from var_src_build_params
    # only keep and rename the following columns
    # Keep               New Name
    #   bin_param_name:    xml_vset_var
    #   value              xml_vset_src
    #   default_val_type   default_val_type
    var_src_build_params = var_src_build_params.loc[:,['bin_param_name','value','default_val_type']]
    var_src_build_params.columns = ['xml_vset_var','xml_vset_src','default_val_type']
    var_src_build_params.sort_values(by=['default_val_type', 'xml_vset_var'], inplace= True)
    


    #### SHOULD probably put this as an input
    #### that way it can be called from wherever it is located
    #### and you could also add in your own custom version
    var_src_prod_fname= 'product-variable-bin-type-source.csv'
    var_src_prod_fname= path + var_src_prod_fname
    
    # NOTE In the case of a single product_id, a special list is created below
    var_src_products = pd.read_csv(var_src_prod_fname, header= 0, sep= ',')
    
    # strip whitespace from the dataframe
    df_strip(var_src_products)

    # Create xml component columns 
    var_src_products['xml_vset_var'] = var_src_products['var_name'].radd('bin_')
    var_src_products = var_src_products[['xml_vset_var','xml_vset_src','default_val_type']]
    var_src_products.sort_values(by=['default_val_type', 'xml_vset_var'], inplace= True)
    
    # columns in the build_params DataFrame
    build_param_cols = var_src_build_params.columns
 
    # make sure var_src_products has the same columns as 
    # var_src_build_params then select only 
    # those columns from it
    has_columns(var_src_products, build_param_cols)
    var_src_products = var_src_products[build_param_cols]
    
    

    # predefined variables to be used in the model
    # This is currently hard coded 
    var_src_opsweb_derived = [
        ['bin_clck', 'ckv_2059', 'count'],
        ['bin_rcy', 'keytsday_2059', 'recency'],
        ['bin_conv', 'ckv_2060', 'count'],
        ['bin_rcy_conv', 'keytsday_2060', 'recency'],
        ['bin_nbr_bkdstn', 'ckv_2336', 'count'],
        ['bin_rcy_bking', 'ckv_2337', 'recency'],
        ['lst_dstnLev', 'ckv_2338', 'true_factor'],

        ['bin_s_las_vegas_ts_rcy_by_day', 'keytsday_16812', 'recency'],                        
        ['bin_s_las_vegas_ts_rcy_by_hour', 'keyts_16812', 'recency'],                        
        ['bin_s_las_vegas_freq', 'ckv_16812', 'count'],                                                      
        ['bin_s_las_ts_rcy_by_day', 'keytsday_16813', 'recency'],                                    
        ['bin_s_las_ts_rcy_by_hour', 'keyts_16813', 'recency'],                                    
        ['bin_s_las_freq', 'ckv_16813', 'count'],                                                                       
        ['bin_b_las_ts_rcy_by_day', 'keytsday_16814', 'recency'],                                    
        ['bin_b_las_ts_rcy_by_hour', 'keyts_16814', 'recency'],                                    
        ['bin_b_las_freq', 'ckv_16814', 'count'],                                                                        
        ['bin_mgm_ts_rcy_by_day', 'keytsday_16815', 'recency'],                                        
        ['bin_mgm_ts_rcy_by_hour', 'keyts_16815', 'recency'],                                        
        ['bin_mgm_freq', 'ckv_16815', 'count']
      ]
  
    var_src_java = [
        ['adunitid', 'adUnitId', 'true_factor'],
        ['daycampaignimpressions', 'dayCampaignImpressions', 'count'],
        ['dayofweek', 'dayOfWeek', 'true_factor'],
        ['dplev', 'dataProviderId', 'true_factor'],
        ['hourofday', 'hourOfDay', 'true_factor'],
        ['impressionlagminutes', 'cookieCampaignLastImpression', 'recency'],
        ['lifetimecampaignimpressions', 'lifetimeCampaignImpressions', 'count'],
        ['networkid', 'networkId', 'true_factor'],
        ['slot_visibility', 'slotVisibility', 'true_factor'],
        ['metrocodelev', 'metrocode', 'true_factor'],
        ['browserlev', 'browser', 'true_factor'],
        ['oslev', 'os', 'true_factor']
    ]

    var_src_fixed = [
        ['delta', 'ckv_671', 'loyalty'],
        ['gender', 'ckv_487', 'unknown'],
        ['hilton', 'ckv_355', 'loyalty'],
        ['homeowner', 'ckv_488', 'unknown'],
        ['ihg', 'ckv_2083', 'loyalty'],
        ['mrt', 'ckv_411', 'loyalty'],
        ['ritz', 'ckv_1650', 'loyalty'],
        ['united', 'ckv_297', 'loyalty'],
        ['usair', 'ckv_16', 'loyalty'],
        ['virgin', 'ckv_2120', 'loyalty']  
    ]

    var_src_variable = [
        ['domainlev', domain_key_id, 'true_factor'],
        ['bin_rcy_rtg', 'keytsday_rtgpkey', 'recency']
    ]

    var_src_derived = [
        ['country_statelev', 'country|state', 'true_factor']
    ]

    var_src_bk = [
        ['bk_3858', 'ckv_5322', 'true_factor'],                                                              
        ['bk_4240', 'ckv_6190', 'true_factor'],                                                            
        ['bk_4629', 'ckv_7627', 'true_factor'],                                                              
        ['bk_49654', 'ckv_6189', 'true_factor'],                                                          
        ['bk_146414', 'ckv_15488', 'true_factor']
    ]


    # NOTE product_id = category_id - both names are used in our databases
    # Get the category key_id
    category_id = df[df['parameter_name'] == 'product_id']['value'].to_numpy()
    cat_key_id = get_category_key_id(category_id)
      
    
    # For the case of a single product_id. 
    # This will duplicate one on the records in the var_src_products
    # but label it as product_click instead of the actual product/category name
    # ie: bin_car_rental_click == bin_product_click 
    # .        if product_id from build_param.csv (@var_src_build_params) = 2 
    #          because 2 is the product/category id for Car Rental
    
    if cat_key_id:
        
        var_src_prod_click= [
                            ['bin_product_click_count', 'ckv_{}'.format(cat_key_id), 'count'],
                            ['bin_product_click_rcy', 'keytsday_{}'.format(cat_key_id), 'recency']
        ]
    else:
        var_src_prod_click= []
    
    

    # if var_src_prod_click is empty don't include it in @combo_var_src
    # it will casue an error 
    # 
    if len(var_src_prod_click) > 0:
        combo_var_src = np.vstack((
                                    var_src_opsweb_derived
                                    ,var_src_java
                                    ,var_src_fixed
                                    ,var_src_variable
                                    ,var_src_derived
                                    ,var_src_prod_click
                                    ,var_src_bk
                                  ))
    else:
        combo_var_src = np.vstack((
                                    var_src_opsweb_derived
                                    ,var_src_java
                                    ,var_src_fixed
                                    ,var_src_variable
                                    ,var_src_derived
                                    ,var_src_bk
                                  ))

    # Turn combined lists into dataframe
    # Rename the columns 
    var_source_df = pd.DataFrame(combo_var_src) 
    var_source_df.columns = var_src_build_params.columns

    # combine all 3 dataframes together
    # this will DataFrame contains a list of all
    # possible model variables and their XML components
    var_source_df = pd.concat([var_source_df,var_src_build_params,var_src_products], ignore_index = True)
    
    # sort by default_val_type then xml_vset_var both ascending
    var_source_df.sort_values(by=['default_val_type', 'xml_vset_var'], inplace= True)

    # reset the index 
    var_source_df.reset_index(drop= True, inplace= True)
    
    return var_source_df

