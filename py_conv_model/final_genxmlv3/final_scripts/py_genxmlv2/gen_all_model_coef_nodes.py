# gen_all_model_coef_nodes.R
## Input xml_info_df

#' Generate all the coefficient nodes
#' 
#' @param vname_mod_est_df a data.frame containing at least `print_order`, 
#' `var_group` `mod_estimate` and `vname` columns
#' @return a list of coefficient nodes to append to p/transforms/variables
#' @export

import pandas as pd
import lxml.etree as ET
from lxml.builder import E
import numpy as np


def gen_all_model_coef_nodes(xml_info_df
                            , cols_to_check= ['print_order', 'var_group', 'mod_estimate', 'vname']):
    """
    Creates all Coefficient Nodes
    from xml_info_df
    
    Returns: coeff_nodes - a list of all coeff nodes in the correct print order
    
    """

    
    # Asserts columns in cols_to_check are in xml_info_df
    # Will throw error if some are missing
#     has_columns(xml_info_df, cols_to_check)

    # Filter xml_info_df 
    cols_to_keep=['var_group','print_order', 'mod_estimate', 'vname']
    # Sort each varname in var_group by print_order
    # then drop duplicates
    # keeping the 1st appearance
    filtered_df= xml_info_df[cols_to_keep].sort_values(by=['var_group','print_order']).drop_duplicates(subset=['var_group','vname'], keep='first')
    
    # list to store coefficient nodes
    coeff_nodes=[]
    
    # Groupby var_group
    # and iterate over the records in each group
    # creating a Coefficient Node for each variable
    # Store all nodes in coeff_nodes
    
    # DF was sorted above so everything will get created 
    # and saved in the correct order
    for groupby, rows in filtered_df.groupby('var_group'):
        for row_index, row in rows.iterrows():
            # Create Node
            variable= row['vname']
            value= row['mod_estimate']
            # create_coef_node defined in create_xml_node_functions.py
            node= create_coef_node(variable, value)
            # append node to list
            coeff_nodes.append(node) 
            
    return coeff_nodes
            
            

