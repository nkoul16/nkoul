from lxml.builder import E

def create_vset_node(var_name=''
                     ,source_name=''
                     ,default_value = '1'
                     ,vset_suffix = '_vset'
                     ,def_var_suffix = '_default'):
    
    """
    Create and return a Variable-Set node
    """
    var_name= str(var_name)
    vset_name= str(var_name) + vset_suffix
    def_var_name= str(var_name) + def_var_suffix

    vset_node= E( tag = 'variable-set' 
                  , name= str(vset_name)
                  , source_set= str(source_name)
                  , default_variable= str(def_var_name) 
                  , default_value= str(default_value)
                )
    
    return vset_node


def create_vset_match_node(variable_name
                           , match_string
                           , value = '1'):
    """
    Create and return a Variable-Set Match Variable node
    """
    
    node=  E.variable( name = str(variable_name)
                     , match = str(match_string)
                     , value = str(value))

    return node



def create_vset_range_node(variable_name
                           , min_rng 
                           , max_rng 
                           , value = '1'):
    """
    Create and return a Variable-Set Range Variable node
    """
    node= E.variable( name = str(variable_name) 
                         , min = str(min_rng)
                         , max = str(max_rng)
                         , value = str(value)
                        )
    return node


def create_coef_node(variable, value):
    

    """
    Create and return a Coeffiecent node
    """
    node= E.coefficient(variable= str(variable), value= str(value))
    
    return node

