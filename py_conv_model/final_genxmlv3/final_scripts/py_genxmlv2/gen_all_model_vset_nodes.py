# gen_all_model_vset_nodes.R

#' Given a fit object, generates all the variable-set nodes
#' 
#' @param vn_df a variable nodes data.frame containing `vname`, `min_rng`, 
#' `max_rng`, `match`, `default_value`, and `print_order`, `xml_vset_src`, 
#' `xml_vset_src`, `is_default` columns
#' @return a list of variable-set nodes to append to p/transforms/variables
#' @export

import pandas as pd
import lxml.etree as ET
from lxml.builder import E
import numpy as np


def gen_all_model_vset_nodes(xml_info_df,
                             cols_to_check= ['vname', 'min_rng', 'max_rng', 'match'
                                             ,'default_value', 'print_order', 'xml_vset_var'
                                             ,'xml_vset_src', 'is_default', 'var_group']
                            ):
    """
    Given xml_info_df, generates all the variable-set nodes
    
    Returns: a list of variable-set nodes to append to p/transforms/variables node in the XML Skeleton
    """

    # Asserts columns in cols_to_check are in xml_info_df
    # Will throw error if some are missing
#     has_columns(xml_info_df, cols_to_check)
    
    # Filter xml_info_df 
    filter_conditions= (xml_info_df['vname'] != 'BETA_NULL') & (xml_info_df['is_default'] == False)
    keep_cols= ['var_group', 'xml_vset_var', 'xml_vset_src', 'vname', 'min_rng', 'max_rng', 'match', 'default_value']
    filtered_df= xml_info_df[filter_conditions][keep_cols]
    
    # Columns to group filtered_df by
    groupby_cols=['xml_vset_var', 'xml_vset_src', 'default_value']
    
    # List to store all variable_set nodes
    var_set_nodes=[]
    for groupby, rows in filtered_df.groupby(groupby_cols):
        vset_var= groupby[0]
        vset_src= groupby[1]
        default_value= groupby[2]

        # call create_vset_node from create_xml_node_functions.py
        # creates the variable set node
        vset_node= create_vset_node(var_name= vset_var
                                    ,source_name= vset_src
                                    ,default_value= default_value)

        # Add in a comment node with the var_set name + _vset 
        # Don't know if this is really needed in the XML
        # but keeping it in for now
        comment_node= ET.Comment(vset_var + '_vset')
        vset_node.append(comment_node)

        for row_index, row in rows.iterrows():
            vname= row['vname']
            min_rng= row['min_rng']
            max_rng= row['max_rng']
            match= row['match']

            # if min_rng is NOT null then create a range node 
            if ~np.isnan(min_rng):
                node= create_vset_range_node( vname
                                             , min_rng 
                                             , max_rng 
                                             , value = '1')
            # Else create a match node
            else:
                node= create_vset_match_node( vname
                                              , match
                                              , value = '1')
            # append node as child of vset_node
            vset_node.append(node)
            
        # Add all vset_node with children to the list
        var_set_nodes.append(vset_node)

        
    return var_set_nodes
    
