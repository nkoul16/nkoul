# gen_save_model_template_xml.R

#' Generate a model template xml from an xml_info and percentiles data frames
#' 
#' xml_info_df contains all the info except for the percentiles needed
#' to create a model template xml
#' 
#' @param xml_info_df a data frame containing all the info except for the
#' percentiles needed to create a model template xml. 
#' (see create_xml_info_df_*)
#' @param percentiles the quantiles and corresponding model score percentiles
#' @param campaign.id the campaign id
#' @param optimization.policy 'C' or 'V'
#' @param template_name the name for this template
#' @param file output file name
#' @return a model template xml document
#' @export


import pandas as pd
import lxml.etree as ET
from lxml.builder import E
import numpy as np
import re


def gen_save_model_template_xml( xml_info_df
                                , percentiles_df
                                , campaign_id = 'REPLACE_WITH_CAMPAIGN_ID'
                                , optimization_policy = 'REPLACE_WITH_OPTIM_POLICY'
                                , template_name = 'REPLACE_WITH_TEMPLATE_NAME'
                                , file = 'model_template.xml'):

    """
    Creates a model XML 
    
    wrties xml to file_path defined by @file
    
    Returns: None
    
    """
    def add_children_from_list(tree, parent_path='', children=[]):
        """
        searches tree for parent node using xpath
        Appends all elements in children 
        to all elements returned by the xpath query
        
        tree: the XML tree to search
        parent_path: path in the tree to the parent node/s
        children: list of elements to append to parent/s
        
        """
        parents= tree.xpath(parent_path)
        
        for parent in parents:
            for child in children:
                parent.append(child)
        
        
        
    
    #----- Create skeleton
    pmodelXML= create_xml_skeleton( camp_id = campaign_id
                                   , opt_pol = optimization_policy
                                   , template_name = template_name)
  

    # Drop unused variables from xml_info_df 
    xml_info_df= xml_info_df.where(xml_info_df.is_used).dropna(how='all', axis=0)
    
    # Create Child Nodes and append to their Parent Node
    
    # Children: vset_nodes 
    # Parent: /p/transform/variables
    vset_nodes= gen_all_model_vset_nodes(xml_info_df)
    add_children_from_list(pmodelXML, parent_path='/p/transform/variables', children= vset_nodes)
    
    # Children: coefficient_nodes 
    # Parent: /p/prediction/algorithm
    coefficient_nodes= gen_all_model_coef_nodes(xml_info_df)
    add_children_from_list(pmodelXML, parent_path='/p/prediction/algorithm', children= coefficient_nodes)

    # Children: range_nodes 
    # Parent: /p/percentile
    range_nodes= create_range_nodes(percentiles_df)
    add_children_from_list(pmodelXML, parent_path='/p/percentile', children= range_nodes)

    # RTG (retargeting)
    # TODO - combine all rtg nodes in 1 function?
    # rtg_coef_nodes <- create_rtg_coef_nodes()
    
    # Save XML to String and Modify before Writing
    header="""<?xml version="1.0"?>\n\n"""
    xml_string= ET.tostring(pmodelXML, pretty_print=True, encoding= 'utf-8').decode()
    
    # Replace all numbers with floating 0's to be int
    xml_string= re.sub(r'\.0"', '"', xml_string)
    # Add .0 back onto version #
    xml_string= xml_string.replace('p version="2"','p version="2.0"')
    # Replace all nan values with NA
    xml_string= xml_string.replace('"nan"','"NA"')
    # Replace all Spaces/Indents infront of Comment nodes
    xml_string= re.sub(r'.*\<\!--', '<!--', xml_string)


    # Write Header and XML String to FIle
    with open(file, 'w') as f:
        f.write(header)
        f.write(xml_string)
        
 