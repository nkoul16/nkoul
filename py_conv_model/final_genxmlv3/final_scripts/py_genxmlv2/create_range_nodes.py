# create_range_nodes.R

#' Create a 'range' type variable node for a variable-set
#' 
#' For the input,
#' 
#' percentile = (1 - quantile)*100
#' 
#' prediction.score = a list of predicted values (output of predict(fit, ...))
#' that corresponds to the quantile
#' 
#' @param percentiles a data.frame containing at least 
#' 'percentile' and 'prediction_score' columns
#' @param roi a scalar for calculating the 3rd column bid price: roi * p
#' @return a list of 'range' nodes that go under the 
#' 'p/prediction/percentile' node
#' @export

import lxml.etree as ET
from lxml.builder import E
import pandas as pd
import numpy as np

def create_range_nodes( percentiles_df
                        , ROI = np.nan
                        , cols_to_check= ['percentile', 'prediction_score']):
    """
    Creates range nodes for the Percentile node
    
    Returns: pct_range_nodes - a list of all range nodes in the correct print order
    """
    
       
    # Asserts columns in cols_to_check are in xml_info_df
    # Will throw error if some are missing
#     has_columns(percentiles, cols_to_check)

    # Sort percentiles_df to make sure 
    # nodes are created and stored 
    # in the correct order
    sorted_df= percentiles_df.sort_values(by='percentile')
    
    # Create list to store nodes
    pct_range_nodes= []
    # iterate over all rows in percentiles_df
    # create a node for every percentile and 
    # append the node to the list
    for row_index, row in sorted_df.iterrows():
        #v: percentile
        #p: prediction score
        v= row['percentile']
        p= row['prediction_score']
        r= ROI
        if np.isnan(ROI):
            node= E.range(value= str(v), p= str(p))
        else:
            #b: ROI * prediction score
            node= E.range(value= str(v), p= str(p), b= str(r*p))

        pct_range_nodes.append(node)

    return pct_range_nodes
        

