import re 
import pandas as pd

def get_sb_rcy_src(variable_names=[]):
    """
    Get XML variable-set names and source_sets 
    for all recency type variables
    
    Returns: DataFrame containing info necessary to 
             create XML variable-set parent node
    
    """
    
    rcy_hour_pattern= re.compile(r'([a-zA-Z]+_[0-9]+.*_rcy_by_hour$)')
    rcy_day_pattern= re.compile(r'([a-zA-Z]+_[0-9]+.*_rcy_by_day$)')
    
    # find variables with patten like 'abc_123_**_rcy_by_hour' or 'abc_123_**_rcy_by_day'    
    sb_rcy_hour= list(filter(rcy_hour_pattern.search, variable_names))
    sb_rcy_day= list(filter(rcy_day_pattern.search, variable_names))

    # Get XML variable-set sources for each column name
    # replacement done here removes all non-numeirc characters from each string
    # leaving behind the key_id for each variable
    # the XML source prefix is then added onto the key_id
    # This combination becomes the value of the XML variable-sets' source_set
    # example source_set:  source_set="keyts_34521"

    # keyts_ sources
    if len(sb_rcy_hour) > 0:
        src_hour= ['keyts_{}'.format(re.sub(r'[a-zA-Z_]+','',i)) for i in sb_rcy_hour]
    else:
        src_hour= []

    # keytsday_ sources
    if len(sb_rcy_day) > 0:
        src_day= ['keytsday_{}'.format(re.sub(r'[a-zA-Z_]+','',i)) for i in sb_rcy_day]
    else:
        src_day= []

    # combine the lists above to create new lists of 
    # variable-set names
    vset_var_names= np.r_[sb_rcy_hour, sb_rcy_day] 
    # variable-set source_sets
    vset_source_sets= np.r_[src_hour, src_day]

    # Create DataFrame from two lists
    var_source_df_sb= pd.DataFrame({'xml_vset_var': vset_var_names
                                    , 'xml_vset_src': vset_source_sets})
    
    
    # Create a coulmn for XML variable-set deafult_value_type
    # set all entries = 'recency'
    var_source_df_sb['default_val_type']= 'recency'


    return var_source_df_sb


