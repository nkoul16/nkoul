def get_category_key_id(cat_id, category_keys_table= 'category-keys.csv', path='test_files/'):
    """
    get the key_id for the input category id @cat_id
    
    The key_ids are all click_tracking keys. 
    They track clicks from all cookies in a given category. 
    
    Returns key_id for a given category if it exists
    
    """

    # Read in category mapping csv
    category_keys_file = path + category_keys_table
    categories_map = pd.read_csv(category_keys_file, header= 0, sep=',')
    
    # Strip WhiteSpace
    df_strip(categories_map)
    
    if len(cat_id) > 0:
        key_id = categories_map[categories_map['category_id' == cat_id]]['key_id'].to_numpy()
        
        # Only return key_id if it exists
        if len(key_id) > 0: return key_id 